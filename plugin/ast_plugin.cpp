#include "ast_macros.hpp"
#include "game/game.hpp"
#include <IUnityInterface.h>

namespace {
ast::Engine& sGetEngine()
{
	static ast::Engine::Ptr sEngine = ast::Engine::create();
	return *sEngine.get();
}

ast::AsteroidGame& sGetAsteroidGame()
{
	static ast::AsteroidGame sGame;
	return sGame;
}
}  // anonymous namespace

extern "C" {

ASTEROID_API void astEngineInitialize(uint32_t inWidth, uint32_t inHeight)
{
	sGetEngine().initialize({ inWidth, inHeight }, sGetAsteroidGame());
}

ASTEROID_API void astEngineDispose()
{
	sGetEngine().dispose();
}

ASTEROID_API void astEngineUpdate(uint32_t inKeyState)
{
	sGetEngine().update(ast::KeyState(inKeyState));
}

}  // extern "C"