#include "game/game.hpp"
#include "resources/resources_db.hpp"
#include <random>
#include <algorithm>

namespace {

int randomInt(int inMin, int inMax)
{
	static std::random_device rd;
	static std::mt19937 gen(rd());
	std::uniform_int_distribution<> distrib(inMin, inMax);
	return distrib(gen);
}
float randomFloat(float inMin, float inMax)
{
	static std::random_device rd;
	static std::mt19937 gen(rd());
	std::uniform_real_distribution<float> distrib(inMin, inMax);
	return distrib(gen);
}
}  // anonymous namespace

namespace ast {

AsteroidGame::AsteroidGame()
	: mAsteroidsCount(0)
	, mDpiScaling(1.f)
{
}

AsteroidGame::~AsteroidGame() noexcept
{
}

void AsteroidGame::onLoadGame()
{
	getSpritesheet().load(AST_ASTEROID_LARGE_PNG, AST_ASTEROID_LARGE_PNG_SIZE);
	getSpritesheet().load(AST_ASTEROID_MEDIUM_PNG, AST_ASTEROID_MEDIUM_PNG_SIZE);
	getSpritesheet().load(AST_ASTEROID_SMALL_PNG, AST_ASTEROID_SMALL_PNG_SIZE);
	getSpritesheet().load(AST_SHIP_PNG, AST_SHIP_PNG_SIZE);
	getSpritesheet().load(AST_MISSILE_PNG, AST_MISSILE_PNG_SIZE);
	mDpiScaling = std::max(1.f, static_cast<float>(getViewportExtents().mWidth) / 1920.f);
	mAsteroidsCount = 0;
	Actor* lShipActor = spawnActor<Ship>();
	Ship* lShipComponent = lShipActor->getComponent<Ship>();
	setupShip(lShipActor, lShipComponent);
}

void AsteroidGame::onUnloadGame()
{
}

void AsteroidGame::onBeginPlay(uint32_t inActor)
{
	// nothing yet
}

void AsteroidGame::onEndPlay(uint32_t inActor)
{
	Actor* lActor = PoolOf<Actor>::get().lookup(inActor);
	if (Asteroid* lAsteroid = lActor->getComponent<Asteroid>()) {
		--mAsteroidsCount;
	}
}

void AsteroidGame::onCollide(uint32_t inActorA, uint32_t inActorB)
{
	Actor* lActorA = PoolOf<Actor>::get().lookup(inActorA);
	Actor* lActorB = PoolOf<Actor>::get().lookup(inActorB);
	if (Ship* lActorAShipComp = lActorA->getComponent<Ship>()) {
		if (Asteroid* lActorBAsteroidComp = lActorB->getComponent<Asteroid>()) {
			onShipCollidedWithAsteroid(lActorA, lActorAShipComp, lActorB, lActorBAsteroidComp);
		}
	}
	else if (Ship* lActorBShipComp = lActorB->getComponent<Ship>()) {
		if (Asteroid* lActorAAsteroidComp = lActorA->getComponent<Asteroid>()) {
			onShipCollidedWithAsteroid(lActorB, lActorBShipComp, lActorA, lActorAAsteroidComp);
		}
	}
	else if (Missile* lActorAMissileComp = lActorA->getComponent<Missile>()) {
		if (Asteroid* lActorBAsteroidComp = lActorB->getComponent<Asteroid>()) {
			onMissileCollidedWithAsteroid(lActorA, lActorAMissileComp, lActorB, lActorBAsteroidComp);
		}
	}
	else if (Missile* lActorBMissileComp = lActorB->getComponent<Missile>()) {
		if (Asteroid* lActorAAsteroidComp = lActorA->getComponent<Asteroid>()) {
			onMissileCollidedWithAsteroid(lActorB, lActorBMissileComp, lActorA, lActorAAsteroidComp);
		}
	}
}

void AsteroidGame::onUpdate(KeyState inKeyState, float inDeltaTimeSecs)
{
	// Order of updates is not important, transform states are double buffered.
	updateShip(inKeyState, inDeltaTimeSecs);
	updateAsteroids(inDeltaTimeSecs);
	updateMissiles(inDeltaTimeSecs);
}

void AsteroidGame::setupRandomAsteroid(Actor* inAsteroidActor, Asteroid* inAsteroidComponent)
{
	TransformComponent* lTransform = inAsteroidActor->getComponent<TransformComponent>();
	GraphicsComponent* lGraphics = inAsteroidActor->getComponent<GraphicsComponent>();
	PhysicsComponent* lPhysics = inAsteroidActor->getComponent<PhysicsComponent>();
	inAsteroidComponent->mHealth = 3u;
	getSpritesheet().lookup(AST_ASSET_REF(AST_ASTEROID_LARGE_PNG), lGraphics->mSpriteBox);
	lGraphics->mSpriteColor = {255, 255, 255, 255};
	Transform lRawTransform = Transform::IDENTITY();
	lRawTransform.mScale = getSpritesheet().radius(AST_ASSET_REF(AST_ASTEROID_LARGE_PNG)) * mDpiScaling;
	lRawTransform.mPosition.mX = ::randomFloat(-1.f * static_cast<float>(getViewportExtents().mWidth / 2.f) + lRawTransform.mScale, static_cast<float>(getViewportExtents().mWidth) / 2.f - lRawTransform.mScale);
	lRawTransform.mPosition.mY = ::randomFloat(-1.f * static_cast<float>(getViewportExtents().mHeight / 2.f) + lRawTransform.mScale, static_cast<float>(getViewportExtents().mHeight) / 2.f - lRawTransform.mScale);
	lRawTransform.mRotation = ::randomFloat(0.f, ast::deg2rad(360.f));
	lTransform->teleport(lRawTransform);
	lPhysics->mAngularVelocity = ::randomFloat(-1.f * MAX_ASTEROID_ANGULAR_VELOCITY, MAX_ASTEROID_ANGULAR_VELOCITY);
	if (lRawTransform.mPosition.mX < 0.f) {
		lPhysics->mVelocity.mX = ::randomFloat(1.f, MAX_ASTEROID_VELOCITY_MULTIPLIER);
	}
	else {
		lPhysics->mVelocity.mX = ::randomFloat(-1.f, -1.f * MAX_ASTEROID_VELOCITY_MULTIPLIER);
	}
	if (lRawTransform.mPosition.mY < 0.f) {
		lPhysics->mVelocity.mY = ::randomFloat(1.f, MAX_ASTEROID_VELOCITY_MULTIPLIER);
	}
	else {
		lPhysics->mVelocity.mY = ::randomFloat(-1.f, -1.f * MAX_ASTEROID_VELOCITY_MULTIPLIER);
	}
}

void AsteroidGame::setupShip(Actor* inShipActor, Ship* inShipComponent)
{
	TransformComponent* lTransform = inShipActor->getComponent<TransformComponent>();
	GraphicsComponent* lGraphics = inShipActor->getComponent<GraphicsComponent>();
	PhysicsComponent* lPhysics = inShipActor->getComponent<PhysicsComponent>();
	inShipComponent->mNextMissileTimeout.reset(0.f);
	inShipComponent->mVelocityMultiplier = 0.f;
	inShipComponent->mNextAsteroidTimeout.reset(ASTEROID_RESPAWN_TIMEOUT_SECS);
	getSpritesheet().lookup(AST_ASSET_REF(AST_SHIP_PNG), lGraphics->mSpriteBox);
	lGraphics->mSpriteColor = {255, 255, 255, 255};
	lPhysics->mAngularVelocity = 0.f;
	lPhysics->mVelocity.mX = 0.f;
	lPhysics->mVelocity.mY = 0.f;
	Transform lRawTransform = Transform::IDENTITY();
	lRawTransform.mScale = 2.f * mDpiScaling * getSpritesheet().radius(AST_ASSET_REF(AST_SHIP_PNG));
	lTransform->teleport(lRawTransform);
}

void AsteroidGame::setupMissile(Actor* inMissileActor,
								Missile* inMissileComponent,
								const Vec2f& inDirection,
								const Vec2f& inPosition,
								float inRotation)
{
	TransformComponent* lTransform = inMissileActor->getComponent<TransformComponent>();
	GraphicsComponent* lGraphics = inMissileActor->getComponent<GraphicsComponent>();
	PhysicsComponent* lPhysics = inMissileActor->getComponent<PhysicsComponent>();
	inMissileComponent->mDespawnTimeout.reset(MISSILE_DESPAWN_TIMEOUT_SECS);
	getSpritesheet().lookup(AST_ASSET_REF(AST_MISSILE_PNG), lGraphics->mSpriteBox);
	lGraphics->mSpriteColor = {255, 255, 255, 255};
	lPhysics->mAngularVelocity = 0.f;
	lPhysics->mVelocity = inDirection * MISSILE_VELOCITY;
	Transform lRawTransform = Transform::IDENTITY();
	lRawTransform.mPosition = inPosition;
	lRawTransform.mRotation = inRotation;
	lRawTransform.mScale = mDpiScaling * getSpritesheet().radius(AST_ASSET_REF(AST_MISSILE_PNG));
	lTransform->teleport(lRawTransform);
}

void AsteroidGame::updateShip(KeyState inKeyState, float inDeltaTimeSecs)
{
	Ship* lShip = PoolOf<Ship>::get().begin().get();
	Actor* lShipActor = PoolOf<Actor>::get().lookup(lShip->mActorId);
	if (!lShipActor->isAlive()) {
		return;
	}
	TransformComponent* lShipTransform = lShipActor->getComponent<TransformComponent>();
	PhysicsComponent* lShipPhysics = lShipActor->getComponent<PhysicsComponent>();
	lShip->mNextMissileTimeout.update(inDeltaTimeSecs);
	lShip->mNextAsteroidTimeout.update(inDeltaTimeSecs);
	if (lShip->mNextAsteroidTimeout.hasElapsed()) {
		lShip->mNextAsteroidTimeout.reset(ASTEROID_RESPAWN_TIMEOUT_SECS);
		if (mAsteroidsCount < MAX_ASTEROIDS_ON_PLAY) {
			++mAsteroidsCount;
			Actor* lAsteroidActor = spawnActor<Asteroid>();
			Asteroid* lAsteroidComponent = lAsteroidActor->getComponent<Asteroid>();
			setupRandomAsteroid(lAsteroidActor, lAsteroidComponent);
		}
	}
	bool lShootMissile = inKeyState.isDown(EKeys::SPACE) && lShip->mNextMissileTimeout.hasElapsed();
	if (lShootMissile) {
		lShip->mNextMissileTimeout.reset(MISSILE_SPAWN_TIMEOUT_SECS);
		Actor* lMissileActor = spawnActor<Missile>();
		Missile* lMissileComponent = lMissileActor->getComponent<Missile>();
		Vec2f lMissileDir = ast::rotateVector({1.f, 0.f}, lShipTransform->get().mRotation);
		Vec2f lMissilePos = (lMissileDir * lShipTransform->get().mScale) + lShipTransform->get().mPosition;
		setupMissile(lMissileActor,
					 lMissileComponent,
					 lMissileDir,
					 lMissilePos,
					 ast::rotate(lShipTransform->get().mRotation, ast::deg2rad(90.f)));
	}
	if (inKeyState.isDown(EKeys::DOWN) && !inKeyState.isDown(EKeys::UP)) {
		lShip->mVelocityMultiplier = std::max(0.f, lShip->mVelocityMultiplier - 10.f * mDpiScaling);
	}
	else if (inKeyState.isDown(EKeys::UP) && !inKeyState.isDown(EKeys::DOWN)) {
		lShip->mVelocityMultiplier = std::min(MAX_SHIP_VELOCITY_MULTIPLIER, lShip->mVelocityMultiplier + 10.f * mDpiScaling);
	}
	else {
		lShip->mVelocityMultiplier = std::max(0.f, lShip->mVelocityMultiplier - 5.f * mDpiScaling);
	}
	if (inKeyState.isDown(EKeys::LEFT) && !inKeyState.isDown(EKeys::RIGHT)) {
		lShipPhysics->mAngularVelocity = -1.f * MAX_SHIP_ANGULAR_VELOCITY;
	}
	else if (inKeyState.isDown(EKeys::RIGHT) && !inKeyState.isDown(EKeys::LEFT)) {
		lShipPhysics->mAngularVelocity = MAX_SHIP_ANGULAR_VELOCITY;
	}
	else {
		lShipPhysics->mAngularVelocity = 0.f;
	}
	Vec2f lShipDir = ast::rotateVector({1.f, 0.f}, lShipTransform->get().mRotation);
	lShipPhysics->mVelocity = lShipDir * lShip->mVelocityMultiplier;
	if (lShipTransform->get().mPosition.mX + lShipTransform->get().mScale >= static_cast<float>(getViewportExtents().mWidth) / 2.f
		&& lShipPhysics->mVelocity.mX > 0.f) {
		lShipPhysics->mVelocity.mX *= -1.f;
	}
	if (lShipTransform->get().mPosition.mX - lShipTransform->get().mScale <= -1.f * static_cast<float>(getViewportExtents().mWidth) / 2.f
		&& lShipPhysics->mVelocity.mX < 0.f) {
		lShipPhysics->mVelocity.mX *= -1.f;
	}
	if (lShipTransform->get().mPosition.mY + lShipTransform->get().mScale >= static_cast<float>(getViewportExtents().mHeight) / 2.f
		&& lShipPhysics->mVelocity.mY > 0.f) {
		lShipPhysics->mVelocity.mY *= -1.f;
	}
	if (lShipTransform->get().mPosition.mY - lShipTransform->get().mScale <= -1.f * static_cast<float>(getViewportExtents().mHeight) / 2.f
		&& lShipPhysics->mVelocity.mY < 0.f) {
		lShipPhysics->mVelocity.mY *= -1.f;
	}
}

void AsteroidGame::updateAsteroids(float inDeltaTimeSecs)
{
	for (auto it = PoolOf<Asteroid>::get().begin(); it != PoolOf<Asteroid>::get().end(); ++it) {
		Actor* lAsteroidActor = PoolOf<Actor>::get().lookup(it->mActorId);
		if (!lAsteroidActor->isAlive()) {
			continue;
		}
		TransformComponent* lTransform = lAsteroidActor->getComponent<TransformComponent>();
		PhysicsComponent* lPhysics = lAsteroidActor->getComponent<PhysicsComponent>();
		if (lTransform->get().mPosition.mX + lTransform->get().mScale >= static_cast<float>(getViewportExtents().mWidth) / 2.f
			&& lPhysics->mVelocity.mX > 0.f) {
			lPhysics->mVelocity.mX *= -1.f;
		}
		if (lTransform->get().mPosition.mX - lTransform->get().mScale <= -1.f * static_cast<float>(getViewportExtents().mWidth) / 2.f
			&& lPhysics->mVelocity.mX < 0.f) {
			lPhysics->mVelocity.mX *= -1.f;
		}
		if (lTransform->get().mPosition.mY + lTransform->get().mScale >= static_cast<float>(getViewportExtents().mHeight) / 2.f
			&& lPhysics->mVelocity.mY > 0.f) {
			lPhysics->mVelocity.mY *= -1.f;
		}
		if (lTransform->get().mPosition.mY - lTransform->get().mScale <= -1.f * static_cast<float>(getViewportExtents().mHeight) / 2.f
			&& lPhysics->mVelocity.mY < 0.f) {
			lPhysics->mVelocity.mY *= -1.f;
		}
	}
}

void AsteroidGame::updateMissiles(float inDeltaTimeSecs)
{
	for (auto it = PoolOf<Missile>::get().begin(); it != PoolOf<Missile>::get().end(); ++it) {
		Actor* lMissileActor = PoolOf<Actor>::get().lookup(it->mActorId);
		if (!lMissileActor->isAlive()) {
			continue;
		}
		it->mDespawnTimeout.update(inDeltaTimeSecs);
		if (it->mDespawnTimeout.hasElapsed() || isOutOfBounds(lMissileActor->getComponent<TransformComponent>())) {
			despawnActor(it->mActorId);
		}
	}
}

void AsteroidGame::onShipCollidedWithAsteroid(Actor* inShipActor,
											  Ship* inShipComponent,
											  Actor* inAsteroidActor,
											  Asteroid* inAsteroidComponent)
{
	despawnActor(inShipActor->mId);
	for (auto it = PoolOf<Asteroid>::get().begin(); it != PoolOf<Asteroid>::get().end(); ++it) {
		despawnActor(it->mActorId);
	}
	for (auto it = PoolOf<Missile>::get().begin(); it != PoolOf<Missile>::get().end(); ++it) {
		despawnActor(it->mActorId);
	}
	Actor* lShipActor = spawnActor<Ship>();
	Ship* lShipComponent = lShipActor->getComponent<Ship>();
	setupShip(lShipActor, lShipComponent);
}

void AsteroidGame::onMissileCollidedWithAsteroid(Actor* inMissileActor,
												 Missile* inMissileComponent,
												 Actor* inAsteroidActor,
												 Asteroid* inAsteroidComponent)
{
	if (--inAsteroidComponent->mHealth == 0) {
		despawnActor(inAsteroidActor->mId);
	}
	else {
		TransformComponent* lMissileTransform = inMissileActor->getComponent<TransformComponent>();
		TransformComponent* lAsteroidTransform = inAsteroidActor->getComponent<TransformComponent>();
		PhysicsComponent* lAsteroidPhysics = inAsteroidActor->getComponent<PhysicsComponent>();
		GraphicsComponent* lAsteroidGraphics = inAsteroidActor->getComponent<GraphicsComponent>();
		Actor* lSpawnedAsteroidActor = spawnActor<Asteroid>();
		++mAsteroidsCount;
		Asteroid* lSpawnedAsteroidComponent = lSpawnedAsteroidActor->getComponent<Asteroid>();
		PhysicsComponent* lSpawnedAsteroidPhysics = lSpawnedAsteroidActor->getComponent<PhysicsComponent>();
		GraphicsComponent* lSpawnedAsteroidGraphics = lSpawnedAsteroidActor->getComponent<GraphicsComponent>();
		TransformComponent* lSpawnedAsteroidTransform = lSpawnedAsteroidActor->getComponent<TransformComponent>();
		lSpawnedAsteroidComponent->mHealth = inAsteroidComponent->mHealth;
		lSpawnedAsteroidGraphics->mSpriteColor = {255, 255, 255, 255};
		lSpawnedAsteroidPhysics->mAngularVelocity = ::randomFloat(-1.f * MAX_ASTEROID_ANGULAR_VELOCITY, MAX_ASTEROID_ANGULAR_VELOCITY);

		Vec2f lHitDir = lAsteroidTransform->get().mPosition - lMissileTransform->get().mPosition;
		lHitDir = lHitDir / ast::lengthVector(lHitDir);
		float lAngleReflect = ast::deg2rad(::randomFloat(20.f, 90.f));
		Vec2f lLeftDir = ast::rotateVector(lHitDir, -1.f * lAngleReflect);
		Vec2f lRightDir = ast::rotateVector(lHitDir, lAngleReflect);
		lAsteroidPhysics->mVelocity = lLeftDir * ast::lengthVector(lAsteroidPhysics->mVelocity);
		lSpawnedAsteroidPhysics->mVelocity = lRightDir * ast::lengthVector(lAsteroidPhysics->mVelocity);
		Transform lRawTransform = Transform::IDENTITY();
		lRawTransform.mPosition = lAsteroidTransform->get().mPosition;
		lRawTransform.mRotation = ::randomFloat(0.f, ast::deg2rad(360.f));
		if (inAsteroidComponent->mHealth == 2) {
			getSpritesheet().lookup(AST_ASSET_REF(AST_ASTEROID_MEDIUM_PNG), lAsteroidGraphics->mSpriteBox);
			getSpritesheet().lookup(AST_ASSET_REF(AST_ASTEROID_MEDIUM_PNG), lSpawnedAsteroidGraphics->mSpriteBox);
			lRawTransform.mScale = mDpiScaling * getSpritesheet().radius(AST_ASSET_REF(AST_ASTEROID_MEDIUM_PNG));
		}
		else {
			getSpritesheet().lookup(AST_ASSET_REF(AST_ASTEROID_SMALL_PNG), lAsteroidGraphics->mSpriteBox);
			getSpritesheet().lookup(AST_ASSET_REF(AST_ASTEROID_SMALL_PNG), lSpawnedAsteroidGraphics->mSpriteBox);
			lRawTransform.mScale = mDpiScaling * getSpritesheet().radius(AST_ASSET_REF(AST_ASTEROID_SMALL_PNG));
		}
		lSpawnedAsteroidTransform->teleport(lRawTransform);
		lAsteroidTransform->teleport(lRawTransform);
	}
	despawnActor(inMissileActor->mId);
}

bool AsteroidGame::isOutOfBounds(TransformComponent* inTransform) const
{
	return inTransform->get().mPosition.mX + inTransform->get().mScale >= static_cast<float>(getViewportExtents().mWidth) / 2.f
		   || inTransform->get().mPosition.mX - inTransform->get().mScale <= -1.f * static_cast<float>(getViewportExtents().mWidth) / 2.f
		   || inTransform->get().mPosition.mY + inTransform->get().mScale >= static_cast<float>(getViewportExtents().mHeight) / 2.f
		   || inTransform->get().mPosition.mY - inTransform->get().mScale <= -1.f * static_cast<float>(getViewportExtents().mHeight) / 2.f;
}

}  // namespace ast