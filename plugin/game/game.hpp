#pragma once
#include "framework/engine.hpp"
#include "os/clock.hpp"

namespace ast {

struct Ship : public Component
{
	Timer mNextMissileTimeout;
	Timer mNextAsteroidTimeout;
	float mVelocityMultiplier;
};

struct Asteroid : public Component
{	
	uint32_t mHealth;
};

struct Missile : public Component
{
	Timer mDespawnTimeout;;
};

class AsteroidGame : public Game
{
public:
	AsteroidGame();
	~AsteroidGame() noexcept;

private:
	virtual void onLoadGame() override;
	virtual void onUnloadGame() override;
	virtual void onBeginPlay(uint32_t inActor) override;
	virtual void onEndPlay(uint32_t inActor) override;
	virtual void onCollide(uint32_t inActorA, uint32_t inActorB) override;
	virtual void onUpdate(KeyState inKeyState, float inDeltaTimeSecs) override;

	void setupRandomAsteroid(Actor* inAsteroidActor, Asteroid* inAsteroidComponent);
	void setupShip(Actor* inShipActor, Ship* inShipComponent);
	void setupMissile(Actor* inMissileActor, 
						   Missile* inMissileComponent,
						   const Vec2f& inDirection,
						   const Vec2f& inPosition,
						   float inRotation);
	void updateShip(KeyState inKeyState, float inDeltaTimeSecs);
	void updateAsteroids(float inDeltaTimeSecs);
	void updateMissiles(float inDeltaTimeSecs);
	void onShipCollidedWithAsteroid(Actor* inShipActor,
									Ship* inShipComponent,
									Actor* inAsteroidActor,
									Asteroid* inAsteroidComponent);
	void onMissileCollidedWithAsteroid(Actor* inMissileActor,
									   Missile* inMissileComponent,
									   Actor* inAsteroidActor,
									   Asteroid* inAsteroidComponent);
	bool isOutOfBounds(TransformComponent* inTransform) const;

private:
	uint32_t mAsteroidsCount;
	float mDpiScaling;
	static constexpr float MAX_ASTEROID_ANGULAR_VELOCITY = ast::deg2rad(359.f);
	static constexpr float MAX_ASTEROID_VELOCITY_MULTIPLIER = 250.f;
	static constexpr float MAX_SHIP_ANGULAR_VELOCITY = ast::deg2rad(270.f);
	static constexpr float MAX_SHIP_VELOCITY_MULTIPLIER = 500.f;
	static constexpr uint32_t MAX_ASTEROIDS_ON_PLAY = 5;
	static constexpr float ASTEROID_RESPAWN_TIMEOUT_SECS = 1.f;
	static constexpr float MISSILE_SPAWN_TIMEOUT_SECS = 0.3f;
	static constexpr float MISSILE_DESPAWN_TIMEOUT_SECS = 2.f;
	static constexpr float MISSILE_VELOCITY = 1000.f;
};

}  // namespace ast
