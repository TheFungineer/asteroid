#include "framework/engine.hpp"
#include "framework/events.hpp"
#include "framework/component.hpp"
#include "containers/slotmap.hpp"
#include "os/clock.hpp"
#include "os/vmem.hpp"
#include <algorithm>
#include <vector>

namespace {
constexpr float AST_PHYSICS_FIXED_TIMESTEP = 1.f / 75.f;
}  // anonymous namespace

namespace ast {

class EngineImpl : public Engine
{
public:
	EngineImpl();
	~EngineImpl() noexcept;

	virtual void initialize(const Vec2ui& inExtents, Game& inGameLogic) override;
	virtual void dispose() override;
	virtual void update(KeyState inKeyboardState) override;

private:
	PhysicsService::Ptr mPhysicsService;
	GraphicsService::Ptr mGraphicsService;
	MasterClock mClock;
	float mAccumulator;
	KeyState mKeyboardState;
	Game* mGameLogic;
};

EngineImpl::EngineImpl()
	: mAccumulator(0.f)
	, mGameLogic(nullptr)
{
}

EngineImpl::~EngineImpl()
{
	// no inline
}

void EngineImpl::initialize(const Vec2ui& inExtents, Game& inGameLogic)
{
	mPhysicsService = PhysicsService::create();
	mGraphicsService = GraphicsService::create();
	mGameLogic = &inGameLogic;
	mGameLogic->load(inExtents, *mPhysicsService);
	mAccumulator = 0.f;
}

void EngineImpl::dispose()
{
	mGameLogic->unload();
	mGameLogic = nullptr;
	mAccumulator = 0.f;
	mPhysicsService.reset();
	mGraphicsService.reset();
}

void EngineImpl::update(KeyState inKeyboardState)
{
	mKeyboardState = inKeyboardState;
	float lDt = mClock.frame();
	mAccumulator += std::min(0.25f, lDt);
	while (mAccumulator >= AST_PHYSICS_FIXED_TIMESTEP) {
		mGameLogic->update(inKeyboardState, AST_PHYSICS_FIXED_TIMESTEP);
		mPhysicsService->integrate();
		mGameLogic->collideAndFinalize();
		mAccumulator -= AST_PHYSICS_FIXED_TIMESTEP;
	}
	mGraphicsService->render(mAccumulator / AST_PHYSICS_FIXED_TIMESTEP);
}

/*/////////////*/

Engine::Engine()
{
	// no inline
}

Engine::~Engine()
{
	// no inline
}

auto Engine::create() -> Ptr
{
	return std::make_unique<EngineImpl>();
}

/*////////////*/

using OnLoadGame = void (*)(void*);
using OnUnloadGame = void (*)(void*);
using OnBeginPlayFunc = void (*)(void*, uint32_t);
using OnEndPlayFunc = void (*)(void*, uint32_t);
using OnCollideFunc = void (*)(void*, uint32_t, uint32_t);
using OnUpdateFunc = void (*)(void*, KeyState, float);

namespace details {

class GameImpl
	: public IEventObserver<OnCollidedEvent>
	, private NonCopyable
{
public:
	GameImpl(void* inThis,
			 OnLoadGame inLoadGameFunc,
			 OnUnloadGame inUnloadGameFunc,
			 OnBeginPlayFunc inBeginPlayFunc,
			 OnEndPlayFunc inEndPlayFunc,
			 OnCollideFunc inCollideFunc,
			 OnUpdateFunc inUpdateFunc);
	~GameImpl() noexcept;
	void load(const Vec2ui& inScreenExtents, IPhysicsEngine& inPhysicsEngine);
	void unload();
	void update(KeyState inKeyState, float inDeltaTimeSecs);
	void collideAndFinalize();
	void spawnActor(uint32_t inActorId);
	void despawnActor(uint32_t inActorId);
	Spritesheet& getSpritesheet();
	Vec2ui getViewportExtents() const;

	virtual void onEvent(const OnCollidedEvent& inEvent) override;

private:
	void finalizeBeginPlay();
	void finalizeEndPlay();
	void finalizeDestroyActor(Actor* inActor);

private:
	struct CollisionPair
	{
		uint32_t mActorA;
		uint32_t mActorB;
	};

private:
	OnLoadGame mLoadGameFunc;
	OnUnloadGame mUnloadGameFunc;
	OnBeginPlayFunc mBeginPlayFunc;
	OnEndPlayFunc mEndPlayFunc;
	OnCollideFunc mCollideFunc;
	OnUpdateFunc mUpdateFunc;
	void* mThis;

	std::vector<uint32_t> mActorsToEndPlay;
	std::vector<uint32_t> mActorsToBeginPlay;
	std::vector<CollisionPair> mCollisionPairs;
	IPhysicsEngine* mPhysicsEngine;
	Spritesheet::Ptr mSpritesheet;
	Clock mGameClock;
	Vec2ui mViewportExtent;
};

GameImpl::GameImpl(void* inThis,
				   OnLoadGame inLoadGameFunc,
				   OnUnloadGame inUnloadGameFunc,
				   OnBeginPlayFunc inBeginPlayFunc,
				   OnEndPlayFunc inEndPlayFunc,
				   OnCollideFunc inCollideFunc,
				   OnUpdateFunc inUpdateFunc)
	: mLoadGameFunc(inLoadGameFunc)
	, mUnloadGameFunc(inUnloadGameFunc)
	, mBeginPlayFunc(inBeginPlayFunc)
	, mEndPlayFunc(inEndPlayFunc)
	, mCollideFunc(inCollideFunc)
	, mUpdateFunc(inUpdateFunc)
	, mThis(inThis)
	, mPhysicsEngine(nullptr)
	, mViewportExtent(Vec2ui::ZERO())
{
	mActorsToBeginPlay.reserve(64);
	mActorsToEndPlay.reserve(64);
	mCollisionPairs.reserve(64);
}

GameImpl::~GameImpl() noexcept
{
	// no inline
}

void GameImpl::load(const Vec2ui& inScreenExtents, IPhysicsEngine& inPhysicsEngine)
{
	mViewportExtent = inScreenExtents;
	mPhysicsEngine = &inPhysicsEngine;
	mPhysicsEngine->setEventObserver(this);
	mSpritesheet = Spritesheet::create({1024, 1024});
	mLoadGameFunc(mThis);
}

void GameImpl::unload()
{
	mUnloadGameFunc(mThis);
	for (auto it = mActorsToBeginPlay.begin(); it != mActorsToBeginPlay.end(); ++it) {
		Actor* lActor = PoolOf<Actor>::get().lookup(*it);
		if (lActor) {
			finalizeDestroyActor(lActor);
			PoolOf<Actor>::get().remove(*it);
		}
	}
	mActorsToBeginPlay.clear();
	mActorsToEndPlay.clear();
	for (auto it = PoolOf<Actor>::get().begin(); it != PoolOf<Actor>::get().end(); ++it) {
		mActorsToEndPlay.push_back(it->mId);
	}
	finalizeEndPlay();
	mSpritesheet.reset();
	mPhysicsEngine->setEventObserver(nullptr);
	mPhysicsEngine = nullptr;
}

Vec2ui GameImpl::getViewportExtents() const
{
	return mViewportExtent;
}

void GameImpl::update(KeyState inKeyState, float inDeltaTimeSecs)
{
	mGameClock.timeStep(inDeltaTimeSecs);
	mUpdateFunc(mThis, inKeyState, mGameClock.elapsed());
}

void GameImpl::collideAndFinalize()
{
	for (auto it = mCollisionPairs.begin(); it != mCollisionPairs.end(); ++it) {
		Actor* lActorA = PoolOf<Actor>::get().lookup(it->mActorA);
		Actor* lActorB = PoolOf<Actor>::get().lookup(it->mActorB);
		if (lActorA && lActorB && lActorA->mIsAlive && lActorB->mIsAlive) {
			mCollideFunc(mThis, it->mActorA, it->mActorB);
		}
	}
	mCollisionPairs.clear();
	finalizeEndPlay();
	finalizeBeginPlay();
}

void GameImpl::spawnActor(uint32_t inActorId)
{
	Actor* lActor = PoolOf<Actor>::get().lookup(inActorId);
	if (lActor) {
		lActor->mIsAlive = false;
		lActor->mTransformComponentId = PoolOf<TransformComponent>::get().add();
		static_cast<Component*>(lActor->getComponent<TransformComponent>())->mActorId = inActorId;
		lActor->mPhysicsComponentId = PoolOf<PhysicsComponent>::get().add();
		static_cast<Component*>(lActor->getComponent<PhysicsComponent>())->mActorId = inActorId;
		lActor->getComponent<PhysicsComponent>()->mIndexerId = -1;
		lActor->getComponent<PhysicsComponent>()->mClock = &mGameClock;
		lActor->mGraphicsComponentId = PoolOf<GraphicsComponent>::get().add();
		static_cast<Component*>(lActor->getComponent<GraphicsComponent>())->mActorId = inActorId;
		mActorsToBeginPlay.push_back(inActorId);
	}
}

void GameImpl::despawnActor(uint32_t inActorId)
{
	Actor* lActor = PoolOf<Actor>::get().lookup(inActorId);
	if (lActor) {
		auto it = std::find(mActorsToBeginPlay.begin(), mActorsToBeginPlay.end(), inActorId);
		if (it != mActorsToBeginPlay.end()) {
			mActorsToBeginPlay.erase(it);
		}
		lActor->mIsAlive = false;
		mActorsToEndPlay.push_back(inActorId);
	}
}

Spritesheet& GameImpl::getSpritesheet()
{
	return *mSpritesheet;
}

void GameImpl::onEvent(const OnCollidedEvent& inEvent)
{
	mCollisionPairs.push_back({inEvent.mActorA, inEvent.mActorB});
}

void GameImpl::finalizeBeginPlay()
{
	for (auto it = mActorsToBeginPlay.begin(); it != mActorsToBeginPlay.end(); ++it) {
		Actor* lActor = PoolOf<Actor>::get().lookup(*it);
		if (lActor) {
			lActor->mIsAlive = true;
			mBeginPlayFunc(mThis, *it);
			mPhysicsEngine->registerPhysicsComponent(lActor->mPhysicsComponentId);
		}
	}
	mActorsToBeginPlay.clear();
}

void GameImpl::finalizeEndPlay()
{
	for (auto it = mActorsToEndPlay.begin(); it != mActorsToEndPlay.end(); ++it) {
		Actor* lActor = PoolOf<Actor>::get().lookup(*it);
		if (lActor) {
			mEndPlayFunc(mThis, *it);
			mPhysicsEngine->unregisterPhysicsComponent(lActor->mPhysicsComponentId);
			finalizeDestroyActor(lActor);
		}
	}
	mActorsToEndPlay.clear();
}

void GameImpl::finalizeDestroyActor(Actor* inActor)
{
	PoolOf<PhysicsComponent>::get().remove(inActor->mPhysicsComponentId);
	PoolOf<GraphicsComponent>::get().remove(inActor->mGraphicsComponentId);
	PoolOf<TransformComponent>::get().remove(inActor->mTransformComponentId);
	ast::behaviourDestroy(*inActor->mBehaviourComponentId);
	inActor->mBehaviourComponentId.reset();
	PoolOf<Actor>::get().remove(inActor->mId);
}

}  // namespace details

/*////////////*/

Game::Game()
	: mPimpl(std::make_unique<details::GameImpl>(this,
												 sOnLoadGame,
												 sOnUnloadGame,
												 sOnBeginPlay,
												 sOnEndPlay,
												 sOnCollide,
												 sOnUpdate))
{
}

Game::~Game() noexcept
{
	// no inline
}

void Game::load(const Vec2ui& inScreenExtents, IPhysicsEngine& inPhysicsEngine)
{
	mPimpl->load(inScreenExtents, inPhysicsEngine);
}

void Game::unload()
{
	mPimpl->unload();
}

void Game::update(KeyState inKeyState, float inDeltaTimeSecs)
{
	mPimpl->update(inKeyState, inDeltaTimeSecs);
}

void Game::collideAndFinalize()
{
	mPimpl->collideAndFinalize();
}

void Game::spawnActorImpl(uint32_t inActorId)
{
	mPimpl->spawnActor(inActorId);
}

void Game::despawnActor(uint32_t inActorId)
{
	mPimpl->despawnActor(inActorId);
}

Spritesheet& Game::getSpritesheet()
{
	return mPimpl->getSpritesheet();
}

Vec2ui Game::getViewportExtents() const
{
	return mPimpl->getViewportExtents();
}

void Game::sOnLoadGame(void* inThis)
{
	static_cast<Game*>(inThis)->onLoadGame();
}

void Game::sOnUnloadGame(void* inThis)
{
	static_cast<Game*>(inThis)->onUnloadGame();
}

void Game::sOnBeginPlay(void* inThis, uint32_t inActor)
{
	static_cast<Game*>(inThis)->onBeginPlay(inActor);
}

void Game::sOnEndPlay(void* inThis, uint32_t inActor)
{
	static_cast<Game*>(inThis)->onEndPlay(inActor);
}

void Game::sOnCollide(void* inThis, uint32_t inActorA, uint32_t inActorB)
{
	static_cast<Game*>(inThis)->onCollide(inActorA, inActorB);
}

void Game::sOnUpdate(void* inThis, KeyState inKeyState, float inDeltaTimeSecs)
{
	static_cast<Game*>(inThis)->onUpdate(inKeyState, inDeltaTimeSecs);
}

}  // namespace ast