#include "framework/spritesheet.hpp"
#include <algorithm>
#include <cassert>
#include <vector>

extern int decodePNG(std::vector<unsigned char>& out_image, unsigned long& image_width, unsigned long& image_height, const unsigned char* in_png, size_t in_size, bool convert_to_rgba32 = true);

extern "C" {
typedef struct _AstLoadTextureCallbackData
{
	void* mThis;
	uint32_t mWidth;
	uint32_t mHeight;
	uint32_t mOffsetX;
	uint32_t mOffsetY;
	void* mData;
} AstLoadTextureCallbackData;

typedef void (*AstLoadTextureCallback)(AstLoadTextureCallbackData*);

static AstLoadTextureCallback sLoadTextureCallback;
static void* sLoadTextureCallbackUserData;

ASTEROID_API void astSetLoadTextureCallback(void* inThis, AstLoadTextureCallback inCallback)
{
	sLoadTextureCallbackUserData = inThis;
	sLoadTextureCallback = inCallback;
}
}  // extern "C"

namespace ast {

class SpritesheetImpl : public Spritesheet
{
public:
	explicit SpritesheetImpl(const Vec2ui& inExtents);
	~SpritesheetImpl() noexcept = default;
	virtual bool load(const unsigned char* inPngData, size_t inPngDataLen) override;
	virtual bool lookup(AssetRef inAssetRef, Box2Df& outBox) const override;
	virtual float radius(AssetRef inAssetRef) const override;

private:
	bool add(AssetRef inAssetRef, const Vec2ui& inDims);

private:
	struct SpriteEntry
	{
		AssetRef mAssetRef;
		Box2Df mBox;
	};
	std::vector<SpriteEntry> mSprites;
	uint32_t mHeadX;
	uint32_t mHeadY;
	uint32_t mNextY;
	Vec2ui mExtents;
};

SpritesheetImpl::SpritesheetImpl(const Vec2ui& inExtents)
	: mHeadX(4)
	, mHeadY(4)
	, mNextY(0)
	, mExtents(inExtents)
{
	mSprites.reserve(16);
}

bool SpritesheetImpl::load(const unsigned char* inPngData, size_t inPngDataLen)
{
	std::vector<unsigned char> lDecoded;
	unsigned long lWidth;
	unsigned long lHeight;
	if (0 == ::decodePNG(lDecoded, lWidth, lHeight, inPngData, inPngDataLen)) {
		if (add(reinterpret_cast<AssetRef>(inPngData), {lWidth, lHeight})) {
			Box2Df lBox;
			lookup(reinterpret_cast<AssetRef>(inPngData), lBox);
			::AstLoadTextureCallbackData lCallbackData;
			lCallbackData.mWidth = lWidth;
			lCallbackData.mHeight = lHeight;
			lCallbackData.mOffsetX = static_cast<uint32_t>(std::round(lBox.mOffset.mX * static_cast<float>(mExtents.mWidth) - 0.5f));
			lCallbackData.mOffsetY = static_cast<uint32_t>(std::round(lBox.mOffset.mY * static_cast<float>(mExtents.mHeight) - 0.5f));
			lCallbackData.mData = lDecoded.data();
			lCallbackData.mThis = sLoadTextureCallbackUserData;
			sLoadTextureCallback(&lCallbackData);
			return true;
		}
	}
	return false;
}

bool SpritesheetImpl::add(AssetRef inAssetRef, const Vec2ui& inDims)
{
	Box2Df lOutBox;
	bool lAlreadyAdded = this->lookup(inAssetRef, lOutBox);
	if (lAlreadyAdded) {
		return true;
	}
	if (mHeadX + inDims.mWidth + 4 > mExtents.mWidth) {
		if (mHeadY + mNextY > mExtents.mHeight) {
			assert(false);
			return false;
		}
		mHeadY += mNextY;
		mHeadX = 4;
		mNextY = 0;
	}
	lOutBox = {{(static_cast<float>(mHeadX) + 0.5f) / static_cast<float>(mExtents.mWidth),
				(static_cast<float>(mHeadY) + 0.5f) / static_cast<float>(mExtents.mHeight)},
			   {(static_cast<float>(inDims.mWidth) - 1.f) / static_cast<float>(mExtents.mWidth),
				(static_cast<float>(inDims.mHeight) - 1.f) / static_cast<float>(mExtents.mHeight)}};
	mHeadX += inDims.mWidth + 4;
	mNextY = std::max(mNextY, inDims.mHeight + 4);
	mSprites.push_back({inAssetRef, lOutBox});
	return true;
}

bool SpritesheetImpl::lookup(AssetRef inAssetRef, Box2Df& outBox) const
{
	auto it = std::find_if(mSprites.cbegin(),
						   mSprites.cend(),
						   [lQueriedAssetRef = inAssetRef](const SpriteEntry& inEntry) -> bool {
							   return lQueriedAssetRef == inEntry.mAssetRef;
						   });
	if (it != mSprites.cend()) {
		outBox = it->mBox;
		return true;
	}
	return false;
}

float SpritesheetImpl::radius(AssetRef inAssetRef) const
{
	Box2Df lBox;
	if (!lookup(inAssetRef, lBox)) {
		return 0.f;
	}
	Vec2f lSize = lBox.mSize;
	lSize.mWidth = (lSize.mWidth * static_cast<float>(mExtents.mWidth) + 1.f) / 2.f;
	lSize.mHeight = (lSize.mHeight * static_cast<float>(mExtents.mHeight) + 1.f) / 2.f;
	//lSize.mWidth /= static_cast<float>(inViewportExtents.mWidth);
	//lSize.mHeight /= static_cast<float>(inViewportExtents.mHeight);
	//lSize.mWidth *= inWorldSize.mWidth;
	//lSize.mHeight *= inWorldSize.mHeight;
	return std::max(lSize.mWidth, lSize.mHeight);
}

/*/////////*/

Spritesheet::Spritesheet()
{
	// no inline
}

Spritesheet::~Spritesheet() noexcept
{
	// no inline
}

auto Spritesheet::create(const Vec2ui& inExtents) -> Ptr
{
	return std::make_unique<SpritesheetImpl>(inExtents);
}

}  // namespace ast