#pragma once
#include "ast_macros.hpp"
#include "framework/math.hpp"
#include <memory>

namespace ast {

class Spritesheet
{
public:
	using Ptr = std::unique_ptr<Spritesheet>;

public:
	virtual ~Spritesheet() noexcept;
	virtual bool load(const unsigned char* inPngData, size_t inPngDataLen) = 0;
	virtual bool lookup(AssetRef inAssetRef, Box2Df& outBox) const = 0;
	virtual float radius(AssetRef inAssetRef) const = 0;

	static Ptr create(const Vec2ui& inExtents);

protected:
	Spritesheet();
};

}  // namespace ast
