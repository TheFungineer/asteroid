#include "framework/physics.hpp"
#include "framework/engine.hpp"
#include "framework/transform.hpp"
#include "framework/graphics.hpp"
#include <box2d/b2_dynamic_tree.h>
#include <algorithm>

namespace {

::b2AABB astMakeB2AABB(const ast::Vec2f& inPosition, float inRadius)
{
	::b2AABB lBox;
	lBox.lowerBound = {inPosition.mX - inRadius, inPosition.mY - inRadius};
	lBox.upperBound = {inPosition.mX + inRadius, inPosition.mY + inRadius};
	return lBox;
}

::b2Vec2 astMakeB2Vec2(const ast::Vec2f& inVec)
{
	return ::b2Vec2(inVec.mX, inVec.mY);
}

}  // anonymous namespace

namespace ast {

/*//////////////////////////*/
class PhysicsServiceImpl : public PhysicsService
{
public:
	PhysicsServiceImpl();
	~PhysicsServiceImpl() noexcept;

	virtual void integrate() override;
	virtual void registerPhysicsComponent(uint32_t inComponentId) override;
	virtual void unregisterPhysicsComponent(uint32_t inComponentId) override;
	virtual void setEventObserver(IEventObserver<OnCollidedEvent>* inObserver) override;

	bool QueryCallback(int32 proxyId);	// ::b2DynamicTree callback

private:
	EventNotifier<OnCollidedEvent> mCollisionEventNotifier;
	::b2DynamicTree mCollidersSpaceIndexer;
	uint32_t mCurrentQueriedActorId;
	int32_t mCurrentQueriedIndexerId;
};

PhysicsServiceImpl::PhysicsServiceImpl()
	: mCurrentQueriedActorId(AST_INVALID_ID)
	, mCurrentQueriedIndexerId(-1)
{
}

PhysicsServiceImpl::~PhysicsServiceImpl() noexcept
{
}

void PhysicsServiceImpl::integrate()
{
	for (auto it = PoolOf<PhysicsComponent>::get().begin(); it != PoolOf<PhysicsComponent>::get().end(); ++it) {
		Actor* lActor = PoolOf<Actor>::get().lookup(it->mActorId);
		if (!lActor || !lActor->isAlive() || it->mClock == nullptr) {
			continue;
		}
		TransformComponent* lTransform = PoolOf<TransformComponent>::get().lookup(lActor->getTransformComponentId());
		if (!lTransform) {
			continue;
		}
		float lDeltaTime = it->mClock->elapsed();
		Transform lNewTransform;
		lNewTransform.mPosition = lTransform->get().mPosition + it->mVelocity * lDeltaTime;
		lNewTransform.mRotation = ast::rotate(lTransform->get().mRotation, it->mAngularVelocity * lDeltaTime);
		lNewTransform.mScale = lTransform->get().mScale;
		if (it->mIndexerId >= 0) {
			Vec2f lDelta = lNewTransform.mPosition - lTransform->get().mPosition;
			mCollidersSpaceIndexer.MoveProxy(it->mIndexerId, ::astMakeB2AABB(lNewTransform.mPosition, lNewTransform.mScale), ::astMakeB2Vec2(lDelta));
		}
		lTransform->updatePrePhysics(lNewTransform);
	}
	TransformComponent::swap();
	for (auto it = PoolOf<PhysicsComponent>::get().begin(); it != PoolOf<PhysicsComponent>::get().end(); ++it) {
		Actor* lEntity = PoolOf<Actor>::get().lookup(it->mActorId);
		if (!lEntity || !lEntity->isAlive()) {
			continue;
		}
		TransformComponent* lTransform = PoolOf<TransformComponent>::get().lookup(lEntity->getTransformComponentId());
		if (!lTransform) {
			continue;
		}
		if (it->mIndexerId >= 0) {
			mCurrentQueriedActorId = it->mActorId;
			mCurrentQueriedIndexerId = it->mIndexerId;
			mCollidersSpaceIndexer.Query<PhysicsServiceImpl>(this, ::astMakeB2AABB(lTransform->get().mPosition, lTransform->get().mScale));
			mCurrentQueriedActorId = AST_INVALID_ID;
			mCurrentQueriedIndexerId = -1;
		}
	}
}

void PhysicsServiceImpl::registerPhysicsComponent(uint32_t inComponentId)
{
	PhysicsComponent* lPhysicsComponent = PoolOf<PhysicsComponent>::get().lookup(inComponentId);
	if (!lPhysicsComponent) {
		return;
	}
	Actor* lEntity = PoolOf<Actor>::get().lookup(lPhysicsComponent->mActorId);
	if (!lEntity) {
		return;
	}
	TransformComponent* lTransformComponent = PoolOf<TransformComponent>::get().lookup(lEntity->getTransformComponentId());
	if (!lTransformComponent) {
		return;
	}
	if (lPhysicsComponent->mIndexerId < 0) {
		lPhysicsComponent->mIndexerId = mCollidersSpaceIndexer.CreateProxy(::astMakeB2AABB(lTransformComponent->get().mPosition, lTransformComponent->get().mScale),
																		   reinterpret_cast<void*>(static_cast<intptr_t>(lPhysicsComponent->mId)));
	}
}

void PhysicsServiceImpl::unregisterPhysicsComponent(uint32_t inComponentId)
{
	PhysicsComponent* lPhysicsComponent = PoolOf<PhysicsComponent>::get().lookup(inComponentId);
	if (!lPhysicsComponent) {
		return;
	}
	if (lPhysicsComponent->mIndexerId >= 0) {
		mCollidersSpaceIndexer.DestroyProxy(lPhysicsComponent->mIndexerId);
		lPhysicsComponent->mIndexerId = -1;
	}
}

bool PhysicsServiceImpl::QueryCallback(int32 inProxyId)
{
	if (mCurrentQueriedIndexerId < inProxyId) {
		uint32_t lComponentId = static_cast<uint32_t>(reinterpret_cast<intptr_t>(mCollidersSpaceIndexer.GetUserData(inProxyId)));
		PhysicsComponent* lComponent = PoolOf<PhysicsComponent>::get().lookup(lComponentId);
		if (lComponent) {
			TransformComponent* lQueriedTransform = PoolOf<TransformComponent>::get().lookup(mCurrentQueriedActorId);
			TransformComponent* lTargetTransform = PoolOf<TransformComponent>::get().lookup(lComponent->mActorId);
			if (lQueriedTransform && lTargetTransform) {
				float lDistance = ast::lengthVector(lQueriedTransform->get().mPosition - lTargetTransform->get().mPosition);
				if (lDistance <= 0.75f * (lQueriedTransform->get().mScale + lTargetTransform->get().mScale)) {
					OnCollidedEvent lEvent = { mCurrentQueriedActorId, lComponent->mActorId };
					mCollisionEventNotifier.notifyEvent(lEvent);
				}
			}
		}
	}
	return true;
}

void PhysicsServiceImpl::setEventObserver(IEventObserver<OnCollidedEvent>* inObserver)
{
	mCollisionEventNotifier.setEventObserver(inObserver);
}

/*///////////////////////////*/

PhysicsService::PhysicsService()
{
	// no inline
}

PhysicsService::~PhysicsService()
{
	// no inline
}

auto PhysicsService::create() -> Ptr
{
	return std::make_unique<PhysicsServiceImpl>();
}

}  // namespace ast
