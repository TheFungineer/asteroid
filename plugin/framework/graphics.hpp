#pragma once
#include "ast_macros.hpp"
#include "framework/component.hpp"
#include "framework/math.hpp"
#include <cstdint>
#include <cstddef>
#include <memory>

namespace ast {

struct GraphicsComponent : public Component
{
	Box2Df mSpriteBox;
	Color mSpriteColor;
};

class GraphicsService : private NonCopyable
{
public:
	using Ptr = std::unique_ptr<GraphicsService>;

public:
	virtual ~GraphicsService() noexcept;

	virtual void render(float inAlpha) = 0;

	static Ptr create();

protected:
	GraphicsService();
};

}  // namespace ast
