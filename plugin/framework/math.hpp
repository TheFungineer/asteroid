#pragma once
#include "ast_macros.hpp"
#include <cstdint>
#include <cmath>

namespace ast {
namespace details {
template <class T>
struct _Vec2
{
	union
	{
		struct
		{
			T mX;
			T mY;
		};
		struct
		{
			T mWidth;
			T mHeight;
		};
	};
	static _Vec2<T> ZERO() { return {T{0}, T{0}}; }
};

template <class T>
inline _Vec2<T> operator*(const _Vec2<T>& inLeft, float inRight)
{
	return {inLeft.mX * inRight, inLeft.mY * inRight};
}

template <class T>
inline _Vec2<T> operator/(const _Vec2<T>& inLeft, float inRight)
{
	return { inLeft.mX / inRight, inLeft.mY / inRight };
}

template <class T>
inline _Vec2<T> operator+(const _Vec2<T>& inLeft, const _Vec2<T>& inRight)
{
	return {inLeft.mX + inRight.mX, inLeft.mY + inRight.mY};
}

template <class T>
inline _Vec2<T> operator-(const _Vec2<T>& inLeft, const _Vec2<T>& inRight)
{
	return {inLeft.mX - inRight.mX, inLeft.mY - inRight.mY};
}

template <class T>
struct _Box2D
{
	_Vec2<T> mOffset;
	_Vec2<T> mSize;

	static _Box2D<T> EMPTY() { return {_Vec2<T>::ZERO(), _Vec2<T>::ZERO()}; }
};

}  // namespace details

using Vec2f = ast::details::_Vec2<float>;
using Vec2ui = ast::details::_Vec2<uint32_t>;
using Box2Df = ast::details::_Box2D<float>;
using Box2Dui = ast::details::_Box2D<uint32_t>;

struct Transform
{
	Vec2f mPosition;
	float mRotation;
	float mScale;
	static Transform IDENTITY() { return Transform{{0.f, 0.f}, 0.f, 1.f}; }
};

constexpr float AST_PI = 3.141592f;

AST_FORCEINLINE constexpr float deg2rad(float inDeg)
{
	return inDeg * (AST_PI / 180.f);
}

AST_FORCEINLINE constexpr float rad2deg(float inRad)
{
	return inRad * (180.f / AST_PI);
}

AST_FORCEINLINE float lerp(float inStart, float inEnd, float inAlpha)
{
	return (1.f - inAlpha) * inStart + inAlpha * inEnd;
}

AST_FORCEINLINE float deltaAngle(float inStart, float inEnd)
{
	float lDifference = std::fmod(inEnd - inStart, 2.f * AST_PI);
	return std::fmod(2.f * lDifference, 2.f * AST_PI) - lDifference;
}

AST_FORCEINLINE float rotate(float inStart, float inAmount)
{
	float lDifference = std::fmod(inAmount, 2.f * AST_PI);
	float lDistance = std::fmod(2.f * lDifference, 2.f * AST_PI) - lDifference;
	return inStart + lDistance;
}

AST_FORCEINLINE Vec2f rotateVector(const Vec2f& inVec, float inRotRad)
{
	return {inVec.mX * std::cos(inRotRad) + inVec.mY * std::sin(inRotRad),
			inVec.mX * -1.f * std::sin(inRotRad) + inVec.mY * std::cos(inRotRad)};
}

AST_FORCEINLINE float lengthVector(const Vec2f& inVec)
{
	return std::sqrt(inVec.mX * inVec.mX + inVec.mY * inVec.mY);
}

AST_FORCEINLINE float lerpAngle(float inStart, float inEnd, float inAlpha)
{
	float lDifference = std::fmod(inEnd - inStart, 2.f * AST_PI);
	float lDistance = std::fmod(2.f * lDifference, 2.f * AST_PI) - lDifference;
	return inStart + lDistance * inAlpha;
}

AST_FORCEINLINE Vec2f lerp(const Vec2f& inStart, const Vec2f& inEnd, float inAlpha)
{
	return {ast::lerp(inStart.mX, inEnd.mX, inAlpha), ast::lerp(inStart.mY, inEnd.mY, inAlpha)};
}

AST_FORCEINLINE Transform lerp(const Transform& inStart, const Transform& inEnd, float inAlpha)
{
	return {ast::lerp(inStart.mPosition, inEnd.mPosition, inAlpha),
			ast::lerpAngle(inStart.mRotation, inEnd.mRotation, inAlpha),
			ast::lerp(inStart.mScale, inEnd.mScale, inAlpha)};
}

struct Color
{
	uint8_t mR;
	uint8_t mG;
	uint8_t mB;
	uint8_t mA;
};

}  // namespace ast
