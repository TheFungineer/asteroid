#include "framework/graphics.hpp"
#include "framework/math.hpp"
#include "framework/engine.hpp"
#include "framework/transform.hpp"
#include "containers/slotmap.hpp"
#include <vector>
#include <cstring>

extern "C" {
typedef struct _AstBeginFillRenderBuffersCallbackData
{
	void* mThis;
	uint64_t mObjectCount;
	void* mTransformPtr;
	void* mUvPtr;
	void* mColorPtr;
} AstBeginFillRenderBuffersCallbackData;

typedef struct _AstEndFillRenderBuffersCallbackData
{
	void* mThis;
	uint64_t mObjectCount;
} AstEndFillRenderBuffersCallbackData;

typedef void (*AstBeginFillRenderBuffersCallback)(AstBeginFillRenderBuffersCallbackData*);
typedef void (*AstEndFillRenderBuffersCallback)(AstEndFillRenderBuffersCallbackData*);

static AstBeginFillRenderBuffersCallback sBeginFillRenderBuffersCallback;
static void* sBeginFillRenderBuffersCallbackUserData;
static AstEndFillRenderBuffersCallback sEndFillRenderBuffersCallback;
static void* sEndFillRenderBuffersCallbackUserData;

ASTEROID_API void astSetBeginFillRenderBuffersCallback(void* inThis, AstBeginFillRenderBuffersCallback inCallback)
{
	sBeginFillRenderBuffersCallbackUserData = inThis;
	sBeginFillRenderBuffersCallback = inCallback;
}

ASTEROID_API void astSetEndFillRenderBuffersCallback(void* inThis, AstEndFillRenderBuffersCallback inCallback)
{
	sEndFillRenderBuffersCallbackUserData = inThis;
	sEndFillRenderBuffersCallback = inCallback;
}

}  // extern "C"

namespace ast {

/*//////////////////////////*/

class GraphicsServiceImpl : public GraphicsService
{
public:
	GraphicsServiceImpl() = default;
	~GraphicsServiceImpl() noexcept = default;

	virtual void render(float inAlpha) override;
};

void GraphicsServiceImpl::render(float inAlpha)
{
	AstBeginFillRenderBuffersCallbackData lCallback;
	lCallback.mObjectCount = static_cast<uint64_t>(PoolOf<TransformComponent>::get().count());
	lCallback.mThis = sBeginFillRenderBuffersCallbackUserData;
	lCallback.mTransformPtr = nullptr;
	lCallback.mUvPtr = nullptr;
	lCallback.mColorPtr = nullptr;
	sBeginFillRenderBuffersCallback(&lCallback);
	size_t lCount = 0;
	if (lCallback.mTransformPtr && lCallback.mUvPtr && lCallback.mColorPtr) {
		for (auto it = PoolOf<TransformComponent>::get().begin(); it != PoolOf<TransformComponent>::get().end(); ++it) {
			Actor* lEntity = PoolOf<Actor>::get().lookup(it->mActorId);
			if (!lEntity || !lEntity->isAlive()) {
				continue;
			}
			GraphicsComponent* lGraphicsComponent = lEntity->getComponent<GraphicsComponent>();
			if (!lGraphicsComponent) {
				continue;
			}
			Transform lInterpXform = it->interpolate(inAlpha);
			float* lTransformArray = (reinterpret_cast<float*>(lCallback.mTransformPtr) + lCount * 4);
			std::memcpy(lTransformArray, &lInterpXform, sizeof(lInterpXform));
			float* lUvArray = (reinterpret_cast<float*>(lCallback.mUvPtr) + lCount * 4);
			std::memcpy(lUvArray, &lGraphicsComponent->mSpriteBox, sizeof(lGraphicsComponent->mSpriteBox));
			float* lColorArray = (reinterpret_cast<float*>(lCallback.mColorPtr) + lCount * 4);
			lColorArray[0] = static_cast<float>(lGraphicsComponent->mSpriteColor.mR) / 255.f;
			lColorArray[1] = static_cast<float>(lGraphicsComponent->mSpriteColor.mG) / 255.f;
			lColorArray[2] = static_cast<float>(lGraphicsComponent->mSpriteColor.mB) / 255.f;
			lColorArray[3] = static_cast<float>(lGraphicsComponent->mSpriteColor.mA) / 255.f;
			++lCount;
		}
	}
	AstEndFillRenderBuffersCallbackData lEndCallback;
	lEndCallback.mObjectCount = static_cast<uint64_t>(lCount);
	lEndCallback.mThis = sEndFillRenderBuffersCallbackUserData;
	sEndFillRenderBuffersCallback(&lEndCallback);
}

/*///////////////////////////*/

GraphicsService::GraphicsService()
{
	// no inline
}

GraphicsService::~GraphicsService()
{
	// no inline
}

auto GraphicsService::create() -> Ptr
{
	return std::make_unique<GraphicsServiceImpl>();
}

}  // namespace ast
