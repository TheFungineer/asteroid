#pragma once
#include "ast_macros.hpp"
#include "framework/math.hpp"
#include "framework/component.hpp"

namespace ast {

struct TransformComponent : public Component
{
private:
	static size_t& getCurrent()
	{
		static size_t sCurrent = 0;
		return sCurrent;
	}

	static size_t next()
	{
		return size_t{ 1ull } - getCurrent();
	}
	static size_t current()
	{
		return getCurrent();
	}

public:
	static void swap()
	{
		getCurrent() = next();
	}
	const Transform& get() const
	{
		return mBuffer[current()];
	}
	void updatePrePhysics(const Transform& inValue)
	{
		mBuffer[next()] = inValue;
	}
	void updatePostPhysics(const Transform& inValue)
	{
		mBuffer[current()] = inValue;
	}
	void teleport(const Transform& inValue)
	{
		mBuffer[current()] = mBuffer[next()] = inValue;
	}
	Transform interpolate(float inAlpha) const
	{
		return ast::lerp(mBuffer[next()],
						 mBuffer[current()],
						 inAlpha);
	}

private:
	Transform mBuffer[2];
};

}  // namespace ast
