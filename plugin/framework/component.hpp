#pragma once
#include "ast_macros.hpp"
#include "containers/slotmap.hpp"
#include <cstdint>
#include <new>

namespace ast {

struct Component : public Trackable
{
	uint32_t mActorId;
};

class TypeErasedBehaviour
{
public:
	template <class T>
	explicit TypeErasedBehaviour(SlotMap<T>& inFactory)
	{
		new (&mSelf_) model<T>(inFactory);
	}
	~TypeErasedBehaviour() noexcept
	{
		std::launder(reinterpret_cast<const TypeErasedBehaviour::concept_t*>(&mSelf_))->~concept_t();
	}
	TypeErasedBehaviour(TypeErasedBehaviour&& inOther) noexcept
	{
		std::launder(reinterpret_cast<const TypeErasedBehaviour::concept_t*>(&inOther.mSelf_))->behaviourMove(&mSelf_);
	}
	TypeErasedBehaviour& operator=(TypeErasedBehaviour&& inOther) noexcept
	{
		std::launder(reinterpret_cast<const TypeErasedBehaviour::concept_t*>(&mSelf_))->~concept_t();
		std::launder(reinterpret_cast<const TypeErasedBehaviour::concept_t*>(&inOther.mSelf_))->behaviourMove(&mSelf_);
		return *this;
	}
	friend void behaviourDestroy(const TypeErasedBehaviour& x);
	template <class T>
	friend T* behaviourCast(const TypeErasedBehaviour& x);

private:
	struct concept_t
	{
		virtual ~concept_t() noexcept = default;
		virtual void behaviourMove(void* inTarget) const noexcept = 0;
		virtual void behaviourDestroy() const = 0;
		virtual void behaviourCast(uint64_t inQueried, void** inoutTarget) const = 0;
	};
	template <typename T>
	struct model : concept_t
	{
	private:
		model()
			: mBehaviourComponentId(AST_INVALID_ID)
		{
		}
	public:
		explicit model(SlotMap<T>& inFactory)
			: mBehaviourComponentId(inFactory.add())
		{
		}
		
		virtual ~model() noexcept
		{
			
		}
		virtual void behaviourMove(void* inTarget) const noexcept override
		{
			auto* lTarget = new (inTarget) model<T>();
			lTarget->mBehaviourComponentId = mBehaviourComponentId;
		}
		virtual void behaviourDestroy() const override
		{
			if (mBehaviourComponentId != AST_INVALID_ID) {
				PoolOf<T>::get().remove(mBehaviourComponentId);
			}
		}
		virtual void behaviourCast(uint64_t inQueried, void** inoutTarget) const override
		{
			if (PoolOf<T>::uid() == inQueried && mBehaviourComponentId != AST_INVALID_ID) {
				*inoutTarget = PoolOf<T>::get().lookup(mBehaviourComponentId);
			}
		}

		uint32_t mBehaviourComponentId;
	};

	std::aligned_storage_t<sizeof(intptr_t) * 2> mSelf_;
};

inline void behaviourDestroy(const TypeErasedBehaviour& x)
{
	std::launder(reinterpret_cast<const TypeErasedBehaviour::concept_t*>(&x.mSelf_))->behaviourDestroy();
}

template <class T>
inline T* behaviourCast(const TypeErasedBehaviour& x)
{
	T* lCasted = nullptr;
	std::launder(reinterpret_cast<const TypeErasedBehaviour::concept_t*>(&x.mSelf_))->behaviourCast(PoolOf<T>::uid(), reinterpret_cast<void**>(&lCasted));
	return lCasted;
}

}  // namespace ast
