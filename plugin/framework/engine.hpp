#pragma once
#include "ast_macros.hpp"
#include "framework/graphics.hpp"
#include "framework/physics.hpp"
#include "framework/transform.hpp"
#include "framework/spritesheet.hpp"
#include "containers/slotmap.hpp"
#include <cstdint>
#include <memory>
#include <optional>

namespace ast {

/*/////////////////////*/

namespace EKeys {
enum type : uint32_t
{
	NONE = 0,
	LEFT = 1 << 0,
	RIGHT = 1 << 1,
	UP = 1 << 2,
	DOWN = 1 << 3,
	SPACE = 1 << 4,
	ESCAPE = 1 << 5,
};
}  // namespace EKeys

class KeyState
{
public:
	KeyState()
		: mState(0u) {}
	KeyState(uint32_t inState)
		: mState(inState) {}
	KeyState(const KeyState& inOther) = default;
	~KeyState() noexcept = default;
	KeyState& operator=(const KeyState& inOther) = default;

	bool isDown(EKeys::type inKey) const
	{
		return (mState & uint32_t{inKey}) != 0;
	}

private:
	uint32_t mState;
};

/*/////////////////*/
class Game;

class Engine : private NonCopyable
{
public:
	using Ptr = std::unique_ptr<Engine>;

public:
	virtual ~Engine() noexcept;
	virtual void initialize(const Vec2ui& inScreenExtents, Game& inGameLogic) = 0;
	virtual void dispose() = 0;
	virtual void update(KeyState inNewKeyboardState) = 0;

	static Ptr create();

protected:
	Engine();
};

namespace details {
class GameImpl;
}  // namespace details

class Actor : public Trackable
{
public:
	Actor& operator=(Actor&& inOther) noexcept
	{
		Trackable::operator=(inOther);
		mBehaviourComponentId = std::move(inOther.mBehaviourComponentId);
		mTransformComponentId = inOther.mTransformComponentId;
		inOther.mTransformComponentId = AST_INVALID_ID;
		mPhysicsComponentId = inOther.mPhysicsComponentId;
		inOther.mPhysicsComponentId = AST_INVALID_ID;
		mGraphicsComponentId = inOther.mGraphicsComponentId;
		inOther.mGraphicsComponentId = AST_INVALID_ID;
		mIsAlive = inOther.mIsAlive;
		inOther.mIsAlive = false;
		return *this;
	}
	template <class T>
	T* getComponent() const
	{
		T* lComponent = nullptr;
		if (mBehaviourComponentId) {
			lComponent = ast::behaviourCast<T>(*mBehaviourComponentId);
		}
		return lComponent;
	}
	template <>
	TransformComponent* getComponent() const
	{
		return PoolOf<TransformComponent>::get().lookup(mTransformComponentId);
	}
	template <>
	PhysicsComponent* getComponent() const
	{
		return PoolOf<PhysicsComponent>::get().lookup(mPhysicsComponentId);
	}
	template <>
	GraphicsComponent* getComponent() const
	{
		return PoolOf<GraphicsComponent>::get().lookup(mGraphicsComponentId);
	}
	uint32_t getTransformComponentId() const
	{
		return mTransformComponentId;
	}
	uint32_t getPhysicsComponentId() const
	{
		return mPhysicsComponentId;
	}
	uint32_t getGraphicsComponentId() const
	{
		return mGraphicsComponentId;
	}
	bool isAlive() const
	{
		return mIsAlive;
	}

private:
	std::optional<TypeErasedBehaviour> mBehaviourComponentId;
	uint32_t mTransformComponentId;
	uint32_t mPhysicsComponentId;
	uint32_t mGraphicsComponentId;
	bool mIsAlive;
	friend class details::GameImpl;
	friend class Game;
};

/*/////////////////////*/

class Game : private NonCopyable
{
public:
	void load(const Vec2ui& inScreenExtents, IPhysicsEngine& inPhysicsEngine);
	void unload();
	void update(KeyState inKeyState, float inDeltaTimeSecs);
	void collideAndFinalize();

protected:
	Game();
	~Game() noexcept;

	template <class T>
	Actor* spawnActor()
	{
		uint32_t lActorId = PoolOf<Actor>::get().add();
		Actor* lActor = PoolOf<Actor>::get().lookup(lActorId);
		lActor->mBehaviourComponentId.emplace(PoolOf<T>::get());
		spawnActorImpl(lActorId);
		static_cast<Component*>(lActor->getComponent<T>())->mActorId = lActorId;
		return lActor;
	}
	void despawnActor(uint32_t inActorId);
	Spritesheet& getSpritesheet();
	Vec2ui getViewportExtents() const;

private:
	virtual void onLoadGame() = 0;
	virtual void onUnloadGame() = 0;
	virtual void onBeginPlay(uint32_t inActor) = 0;
	virtual void onEndPlay(uint32_t inActor) = 0;
	virtual void onCollide(uint32_t inActorA, uint32_t inActorB) = 0;
	virtual void onUpdate(KeyState inKeyState, float inDeltaTimeSecs) = 0;

	static void sOnLoadGame(void* inThis);
	static void sOnUnloadGame(void* inThis);
	static void sOnBeginPlay(void* inThis, uint32_t inActor);
	static void sOnEndPlay(void* inThis, uint32_t inActor);
	static void sOnCollide(void* inThis, uint32_t inActorA, uint32_t inActorB);
	static void sOnUpdate(void* inThis, KeyState inKeyState, float inDeltaTimeSecs);

	void spawnActorImpl(uint32_t inActorId);

private:
	std::unique_ptr<details::GameImpl> mPimpl;
};

}  // namespace ast
