#pragma once
#include "ast_macros.hpp"
#include "framework/math.hpp"
#include "framework/events.hpp"
#include "framework/component.hpp"
#include "os/clock.hpp"
#include <memory>
#include <cstdint>

namespace ast {

struct PhysicsComponent : public Component
{
	Clock* mClock;
	int32_t mIndexerId;
	Vec2f mVelocity;
	float mAngularVelocity;
};

struct OnCollidedEvent
{
	uint32_t mActorA;
	uint32_t mActorB;
};

class IPhysicsEngine : public IEventNotifier<OnCollidedEvent>
{
public:
	virtual void registerPhysicsComponent(uint32_t inComponentId) = 0;
	virtual void unregisterPhysicsComponent(uint32_t inComponentId) = 0;
};

class PhysicsService
	: public IPhysicsEngine
	, private NonCopyable
{
public:
	using Ptr = std::unique_ptr<PhysicsService>;

public:
	virtual ~PhysicsService() noexcept;

	virtual void integrate() = 0;

	static Ptr create();

protected:
	PhysicsService();
};

}  // namespace ast
