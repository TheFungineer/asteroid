#pragma once
#include "ast_macros.hpp"

namespace ast {

template <class TEvent>
class IEventObserver
{
public:
	virtual void onEvent(const TEvent& inEvent) = 0;

protected:
	IEventObserver() = default;
	~IEventObserver() noexcept = default;
};

/*////////////////*/

template <class TEvent>
class IEventNotifier
{
public:
	virtual void setEventObserver(IEventObserver<TEvent>* inObserver) = 0;

protected:
	IEventNotifier() = default;
	~IEventNotifier() noexcept = default;
};

/*////////////////*/

template <class TEvent>
class EventNotifier : public IEventNotifier<TEvent>
{
public:
	EventNotifier()
		: mObserver(nullptr)
	{
	}

	~EventNotifier() noexcept = default;

	virtual void setEventObserver(IEventObserver<TEvent>* inObserver) override
	{
		mObserver = inObserver;
	}

	void notifyEvent(const TEvent& inEvent)
	{
		if (mObserver) {
			mObserver->onEvent(inEvent);
		}
	}

private:
	IEventObserver<TEvent>* mObserver;
};

}  // namespace ast
