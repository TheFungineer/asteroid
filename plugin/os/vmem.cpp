#include "vmem.hpp"

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

namespace ast {

void* vmmap(size_t inLen)
{
#ifdef _WIN32
    return ::VirtualAlloc(0, inLen, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
#else
    // TODO
    return nullptr;
#endif
}

void vmunmap(void* inPtr)
{
#ifdef _WIN32
    ::VirtualFree(inPtr, 0, MEM_RELEASE);
#else
    // TODO
#endif
}

size_t vmgranularity()
{
#ifdef _WIN32
    ::SYSTEM_INFO lInfo = {};
    ::GetSystemInfo(&lInfo);
    return lInfo.dwAllocationGranularity;
#else
    // TODO
    return size_t{ 4096u };
#endif
}

}  // namespace ast
