#pragma once
#include <cstdint>
#include "ast_macros.hpp"

namespace ast {

class MasterClock : private ast::NonCopyable
{
public:
    MasterClock();
    ~MasterClock() noexcept = default;
    void start();
    float frame();

	static float getFrequency();

private:
    uint64_t mTick;
};

/*//////////////*/

class Clock
{
public:
	Clock();
	void timeStep(float inDeltaSecs);
	float elapsed() const;
	void setPaused(bool inIsPaused);
	bool isPaused() const;
	void setTimeScale(float inScale);
	float getTimeScale() const;

private:
	uint64_t mCurrentCycle;
	uint64_t mPreviousCycle;
	float mTimeScale;
	bool mIsPaused;
};

/*//////////////*/

class Timer
{
public:
	void reset(float inDurationSecs);
	void update(float inDeltaSecs);
	bool hasElapsed() const;

private:
	uint64_t mCyclesRemaining;
};

}  // namespace ast
