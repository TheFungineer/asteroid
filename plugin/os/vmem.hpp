#pragma once
#include "ast_macros.hpp"
#include <algorithm>

namespace ast {

constexpr size_t AST_L1_CACHELINE_SIZE = size_t{ 128u };

extern void* vmmap(size_t inLen);
extern void vmunmap(void* inPtr);
extern size_t vmgranularity();

class VMAllocGuard : private NonCopyable
{
	void* mPtr;
public:
	VMAllocGuard()
		: mPtr(nullptr)
	{

	}

	explicit VMAllocGuard(size_t inLen)
		: mPtr(ast::vmmap(inLen))
	{
	}

	~VMAllocGuard() noexcept
	{
		release();
	}

	VMAllocGuard(VMAllocGuard&& inOther) noexcept
		: mPtr(inOther.mPtr)
	{
		inOther.mPtr = nullptr;
	}

	VMAllocGuard& operator=(VMAllocGuard&& inOther) noexcept
	{
		release();
		std::swap(mPtr, inOther.mPtr);
		return *this;
	}

	void release()
	{
		if (mPtr) {
			ast::vmunmap(mPtr);
			mPtr = nullptr;
		}
	}

	void* get() const
	{
		return mPtr;
	}

};

}  // namespace ast
