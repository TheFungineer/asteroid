#include "clock.hpp"
#ifdef _WIN32
#	define WIN32_LEAN_AND_MEAN
#	include <Windows.h>
#endif
#include <cmath>
#include <limits>

namespace {
float getHighResTimerFrequency()
{
#ifdef _WIN32
	::LARGE_INTEGER lFrequency;
	::BOOL res = ::QueryPerformanceFrequency(&lFrequency);
	return static_cast<float>(lFrequency.QuadPart);
#else
	// TODO
#endif
}

uint64_t getHighResTimerTick()
{
#ifdef _WIN32
	::LARGE_INTEGER lTick;
	::BOOL res = ::QueryPerformanceCounter(&lTick);
	return static_cast<uint64_t>(lTick.QuadPart);
#else
	// TODO
#endif
}
}  // anonymous namespace

namespace ast {

float MasterClock::getFrequency()
{
	static float sFrequency = getHighResTimerFrequency();
	return sFrequency;
}

MasterClock::MasterClock()
	: mTick(uint64_t{0ull})
{
}

void MasterClock::start()
{
	mTick = getHighResTimerTick();
}

float MasterClock::frame()
{
	uint64_t lLastTick = mTick;
	start();
	return static_cast<float>(mTick - lLastTick) / getFrequency();
}

/*///////////*/

Clock::Clock()
	: mCurrentCycle(0)
	, mPreviousCycle(0)
	, mTimeScale(0.f)
	, mIsPaused(false)

{
	setTimeScale(1.f);
}

void Clock::timeStep(float inDeltaSecs)
{
	mPreviousCycle = mCurrentCycle;
	if (!mIsPaused) {
		mCurrentCycle += static_cast<uint64_t>(std::round(static_cast<double>(inDeltaSecs)
														  * static_cast<double>(mTimeScale)
														  * static_cast<double>(MasterClock::getFrequency())));
	}
}

float Clock::elapsed() const
{
	return static_cast<float>(static_cast<double>(mCurrentCycle - mPreviousCycle) / static_cast<double>(MasterClock::getFrequency()));
}

void Clock::setPaused(bool inIsPaused)
{
	mIsPaused = inIsPaused;
}

bool Clock::isPaused() const
{
	return mIsPaused;
}

void Clock::setTimeScale(float inScale)
{
	mTimeScale = inScale;
}

float Clock::getTimeScale() const
{
	return mTimeScale;
}

/*///////////////////////*/

void Timer::reset(float inDurationSecs)
{
	mCyclesRemaining = static_cast<uint64_t>(std::round(static_cast<double>(inDurationSecs) * static_cast<double>(MasterClock::getFrequency())));
}

void Timer::update(float inDeltaSecs)
{
	uint64_t lCyclesElapsed = static_cast<uint64_t>(std::round(static_cast<double>(inDeltaSecs)
															   * static_cast<double>(MasterClock::getFrequency())));
	if (lCyclesElapsed <= mCyclesRemaining) {
		mCyclesRemaining -= lCyclesElapsed;
	}
	else {
		mCyclesRemaining = 0;
	}
}

bool Timer::hasElapsed() const
{
	return mCyclesRemaining == 0;
}

}  // namespace ast
