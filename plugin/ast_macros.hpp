#pragma once
#include <cstdint>
#include <limits>

#ifdef _WIN32
#	ifdef AsteroidPlugin_EXPORTS
#		define ASTEROID_API __declspec(dllexport)
#	else
#		define ASTEROID_API __declspec(dllimport)
#	endif
#	define AST_FORCEINLINE __forceinline
#elif __APPLE__
#	error TODO for OSX
#endif

namespace ast {

class NonCopyable
{
public:
	NonCopyable(const NonCopyable&) = delete;
	NonCopyable& operator=(const NonCopyable&) = delete;

protected:
	NonCopyable() = default;
	~NonCopyable() noexcept = default;

	NonCopyable(NonCopyable&&) = default;
	NonCopyable& operator=(NonCopyable&&) = default;
};

using AssetRef = intptr_t;

constexpr uint32_t AST_INVALID_ID = std::numeric_limits<uint32_t>::max();

}  // namespace ast

#define AST_ASSET_REF(addr) \
	reinterpret_cast<ast::AssetRef>(ast::##addr)
