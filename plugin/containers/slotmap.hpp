#pragma once
#include "ast_macros.hpp"
#include "os/vmem.hpp"
#include <cstddef>
#include <cstdint>
#include <new>
#include <limits>
#include <vector>
#include <utility>

namespace ast {

namespace details {

struct Index
{
	uint32_t mId;
	uint16_t mIndex;
	uint16_t mNext;
};

}  // namespace details

/*//////////////////*/

struct Trackable
{
	uint32_t mId;
};

/*//////////////////*/

template <class T>
class SlotMap : private ast::NonCopyable
{
private:
	static constexpr size_t MALLOC_LEN = size_t{1024ull * 1024ull * 1024ull};
	static constexpr uint32_t INDEX_MASK = 0xffff;
	static constexpr uint32_t NEW_OBJECT_ID_ADD = 0x10000;

public:
	static constexpr uint16_t MAX_OBJECTS_COUNT = uint16_t( MALLOC_LEN / (sizeof(T) + sizeof(details::Index)) );

public:
	class Iterator
	{
	public:
		friend void swap(ast::SlotMap<T>::Iterator& first, ast::SlotMap<T>::Iterator& second)
		{
			using std::swap;
			swap(first.mSlotMap, second.mSlotMap);
			swap(first.mItemIdx, second.mItemIdx);
		}

		friend bool operator==(const ast::SlotMap<T>::Iterator& first, const ast::SlotMap<T>::Iterator& second)
		{
			return first.mSlotMap == second.mSlotMap && first.mItemIdx == second.mItemIdx;
		}

		friend bool operator!=(const ast::SlotMap<T>::Iterator& first, const ast::SlotMap<T>::Iterator& second)
		{
			return !(first == second);
		}

		Iterator()
			: mSlotMap(nullptr)
			, mItemIdx(std::numeric_limits<uint16_t>::max())
		{

		}

		explicit Iterator(SlotMap<T>& inSlotMap, uint16_t inItemIdx)
			: mSlotMap(&inSlotMap)
			, mItemIdx(inItemIdx)
		{

		}

		Iterator(const Iterator& inOther)
			: mSlotMap(inOther.mSlotMap)
			, mItemIdx(inOther.mItemIdx)
		{

		}

		~Iterator() noexcept = default;

		Iterator& operator=(Iterator inOther)
		{
			swap(*this, inOther);
			return *this;
		}

		void operator++()
		{
			if (mSlotMap && mItemIdx != std::numeric_limits<uint16_t>::max()) {
				++mItemIdx;
			}
		}

		T* get() const
		{
			if (mSlotMap && mItemIdx != std::numeric_limits<uint16_t>::max()) {
				return mSlotMap->getObject(mItemIdx);
			}
			return nullptr;
		}

		T* operator->() const
		{
			return get();
		}


	private:
		SlotMap<T>* mSlotMap;
		uint16_t mItemIdx;
	};
	friend class ast::SlotMap<T>::Iterator;

public:
	SlotMap()
		: mMalloc(MALLOC_LEN)
	{
		static_assert(std::is_base_of_v<Trackable, T>, "ast::SlotMap template T must inherit from ast::Trackable.");
		void* lHead = mMalloc.get();
		for (size_t i = 0; i < MAX_OBJECTS_COUNT; ++i) {
			new (lHead) T();
			lHead = reinterpret_cast<void*>(reinterpret_cast<intptr_t>(lHead) + sizeof(T));
		}
		for (uint16_t i = 0; i < MAX_OBJECTS_COUNT; ++i) {
			details::Index* lIndex = new (lHead) details::Index();
			lIndex->mId = static_cast<uint32_t>(i);
			lIndex->mNext = i + 1;
			lHead = reinterpret_cast<void*>(reinterpret_cast<intptr_t>(lHead) + sizeof(details::Index));
		}
		mNumObjects = 0;
		mFreelistDequeue = 0;
		mFreelistEnqueue = MAX_OBJECTS_COUNT - 1;
	}

	~SlotMap() noexcept
	{
		void* lHead = mMalloc.get();
		for (size_t i = 0; i < MAX_OBJECTS_COUNT; ++i) {
			std::launder(reinterpret_cast<T*>(lHead))->~T();
			lHead = reinterpret_cast<void*>(reinterpret_cast<intptr_t>(lHead) + sizeof(T));
		}
		for (size_t i = 0; i < MAX_OBJECTS_COUNT; ++i) {
			std::launder(reinterpret_cast<details::Index*>(lHead))->~Index();
			lHead = reinterpret_cast<void*>(reinterpret_cast<intptr_t>(lHead) + sizeof(details::Index));
		}
	}

	bool contains(uint32_t id) const
	{
		details::Index& in = *getIndex(id & INDEX_MASK);
		return in.mId == id && in.mIndex != std::numeric_limits<uint16_t>::max();
	}

	uint16_t count() const
	{
		return mNumObjects;
	}

	T* lookup(uint32_t id)
	{
		if (!contains(id)) {
			return nullptr;
		}
		return getObject(getIndex(id & INDEX_MASK)->mIndex);
	}

	const T* lookup(uint32_t id) const
	{
		if (!contains(id)) {
			return nullptr;
		}
		return getObject(getIndex(id & INDEX_MASK)->mIndex);
	}

	uint32_t add()
	{
		details::Index& in = *getIndex(mFreelistDequeue);
		mFreelistDequeue = in.mNext;
		in.mId += NEW_OBJECT_ID_ADD;
		in.mIndex = mNumObjects++;
		T& o = *getObject(in.mIndex);
		static_cast<Trackable&>(o).mId = in.mId;
		return static_cast<Trackable&>(o).mId;
	}

	void remove(uint32_t id)
	{
		details::Index& in = *getIndex(id & INDEX_MASK);

		T& o = *getObject(in.mIndex);
		o = std::move(*getObject(--mNumObjects));
		getIndex(static_cast<Trackable&>(o).mId & INDEX_MASK)->mIndex = in.mIndex;

		in.mIndex = std::numeric_limits<uint16_t>::max();
		getIndex(mFreelistEnqueue)->mNext = id & INDEX_MASK;
		mFreelistEnqueue = id & INDEX_MASK;
	}

	SlotMap<T>::Iterator begin()
	{
		return SlotMap<T>::Iterator(*this, 0);
	}

	SlotMap<T>::Iterator end()
	{
		return SlotMap<T>::Iterator(*this, mNumObjects);
	}

private:
	T* getObject(uint16_t inIdx) const
	{
		if (inIdx >= MAX_OBJECTS_COUNT) {
			return nullptr;
		}
		void* lAddr = reinterpret_cast<void*>(reinterpret_cast<intptr_t>(mMalloc.get()) + sizeof(T) * inIdx);
		return std::launder(reinterpret_cast<T*>(lAddr));
	}
	details::Index* getIndex(uint32_t inIdx) const
	{
		if (inIdx >= MAX_OBJECTS_COUNT) {
			return nullptr;
		}
		void* lAddr = reinterpret_cast<void*>(reinterpret_cast<intptr_t>(mMalloc.get()) + sizeof(T) * MAX_OBJECTS_COUNT + sizeof(details::Index) * inIdx);
		return std::launder(reinterpret_cast<details::Index*>(lAddr));
	}

private:
	VMAllocGuard mMalloc;
	uint16_t mNumObjects;
	uint16_t mFreelistEnqueue;
	uint16_t mFreelistDequeue;
};

/*/////////////////////*/

template <class T>
struct PoolOf
{
	static SlotMap<T>& get()
	{
		static SlotMap<T> sPool;
		return sPool;
	}
	static uint64_t uid()
	{
		return reinterpret_cast<uint64_t>(&get());
	}
};

}  // namespace ast
