using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Rendering;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;

public class AsteroidMain : MonoBehaviour
{
    /*/////////////////////////////*/
    // Interop & Marshalling
    /*/////////////////////////////*/
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
    private const string AsteroidNativeDLL = "AsteroidPlugin.dll";
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
    private const string AsteroidNativeDLL = "TODO";
#endif
    enum EKeyState : UInt32
    {
        NONE = 0,
        LEFT = 1 << 0,
        RIGHT = 1 << 1,
        UP = 1 << 2,
        DOWN = 1 << 3,
        SPACE = 1 << 4,
        ESCAPE = 1 << 5
    }
    [StructLayout(LayoutKind.Sequential)]
    struct AstBeginFillRenderBuffersCallbackData
    {
        public IntPtr mThis;
        public UInt64 mObjectCount;
        public IntPtr mTransformPtr;
        public IntPtr mUvPtr;
        public IntPtr mColorPtr;
    }
    [StructLayout(LayoutKind.Sequential)]
    struct AstEndFillRenderBuffersCallbackData
    {
        public IntPtr mThis;
        public UInt64 mObjectCount;
    }
    [StructLayout(LayoutKind.Sequential)]
    struct AstLoadTextureCallbackData
    {
        public IntPtr mThis;
        public UInt32 mWidth;
        public UInt32 mHeight;
        public UInt32 mOffsetX;
        public UInt32 mOffsetY;
        public IntPtr mData;
    }

    [DllImport(AsteroidNativeDLL, CallingConvention = CallingConvention.Cdecl)]
    private static extern void astEngineInitialize(UInt32 inWidth, UInt32 inHeight);
    [DllImport(AsteroidNativeDLL, CallingConvention = CallingConvention.Cdecl)]
    private static extern void astEngineDispose();
    [DllImport(AsteroidNativeDLL, CallingConvention = CallingConvention.Cdecl)]
    private static extern void astEngineUpdate(UInt32 inKeyState);
    [DllImport(AsteroidNativeDLL, CallingConvention = CallingConvention.Cdecl)]
    private static extern void astSetBeginFillRenderBuffersCallback(IntPtr inThis, IntPtr inCallback);
    [DllImport(AsteroidNativeDLL, CallingConvention = CallingConvention.Cdecl)]
    private static extern void astSetEndFillRenderBuffersCallback(IntPtr inThis, IntPtr inCallback);
    [DllImport(AsteroidNativeDLL, CallingConvention = CallingConvention.Cdecl)]
    private static extern void astSetLoadTextureCallback(IntPtr inThis, IntPtr inCallback);
    private delegate void OnBeginFillRenderBuffersDelegate(ref AstBeginFillRenderBuffersCallbackData inCallbackData);
    private static OnBeginFillRenderBuffersDelegate sOnBeginFillRenderBuffersDelegate;
    private delegate void OnEndFillRenderBuffersDelegate(ref AstEndFillRenderBuffersCallbackData inCallbackData);
    private static OnEndFillRenderBuffersDelegate sOnEndFillRenderBuffersDelegate;
    private delegate void OnLoadTextureDelegate(ref AstLoadTextureCallbackData inCallbackData);
    private static OnLoadTextureDelegate sOnLoadTextureDelegate;

    /*/////////////////////////////*/
    // Circular CPU/GPU Fenced
    // ComputeBuffers
    /*/////////////////////////////*/
    private class RingBuffer
    {
        private class GraphicsFenceData
        {
            private int mLen;
            private int mPad;
            private GraphicsFence mFence;

            public GraphicsFenceData()
            {
                mLen = 0;
                mPad = 0;
            }
            public void fence()
            {
                mFence = Graphics.CreateGraphicsFence(GraphicsFenceType.CPUSynchronisation, SynchronisationStageFlags.PixelProcessing);
            }
            public void setLen(int inPad, int inLen)
            {
                mLen = inLen;
                mPad = inPad;
            }
            public int updateLen(int inLen)
            {
                int lOldLen = mLen;
                mLen = inLen;
                return lOldLen - inLen;
            }
            public int isDone()
            {
                if (mFence.passed)
                {
                    return mLen + mPad;
                }
                return 0;
            }
        }
        private ComputeBuffer mTransformBuffer;
        private ComputeBuffer mUvBuffer;
        private ComputeBuffer mColorBuffer;
        private int mHead;
        private int mTail;
        private Queue<GraphicsFenceData> mInFlightOps;
        private GraphicsFenceData mPendingOp;

        private const int STRIDE = sizeof(float) * 4;
        private const int MAX_COUNT = 1024 * 1024 / STRIDE;

        public ComputeBuffer getTransformBuffer()
        {
            return mTransformBuffer;
        }

        public ComputeBuffer getUvBuffer()
        {
            return mUvBuffer;
        }

        public ComputeBuffer getColorBuffer()
        {
            return mColorBuffer;
        }

        public void initialize()
        {
            mInFlightOps = new Queue<GraphicsFenceData>();
            mPendingOp = null;
            mTransformBuffer = new ComputeBuffer(MAX_COUNT, STRIDE, ComputeBufferType.Default, ComputeBufferMode.SubUpdates);
            mUvBuffer = new ComputeBuffer(MAX_COUNT, STRIDE, ComputeBufferType.Default, ComputeBufferMode.SubUpdates);
            mColorBuffer = new ComputeBuffer(MAX_COUNT, STRIDE, ComputeBufferType.Default, ComputeBufferMode.SubUpdates);
            mHead = 0;
            mTail = 0;
        }

        public void dispose()
        {
            mTransformBuffer.Release();
            mUvBuffer.Release();
            mColorBuffer.Release();
        }

        public void beginUpdate(int inCount, ref IntPtr inoutTrasformPtr, ref IntPtr inoutUvPtr, ref IntPtr inoutColorPtr)
        {
            if (mPendingOp != null)
            {
                mPendingOp.fence();
                mInFlightOps.Enqueue(mPendingOp);
                mPendingOp = null;
            }
            if (mInFlightOps.Count > 0) 
            {
                var lFront = mInFlightOps.Peek();
                while (lFront.isDone() > 0)
                {
                    mTail = (mTail + lFront.isDone()) % MAX_COUNT;
                    mInFlightOps.Dequeue();
                    if (mInFlightOps.Count == 0)
                    {
                        break;
                    }
                    lFront = mInFlightOps.Peek();
                }
            }
            int lLen = 0;
            int lPad = 0;
            if (mTail <= mHead)
            {
                if (MAX_COUNT - mHead >= inCount)
                {
                    lLen += inCount;
                    unsafe
                    {
                        inoutTrasformPtr = (IntPtr)NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(mTransformBuffer.BeginWrite<float>(mHead * 4, inCount * 4));
                        inoutUvPtr = (IntPtr)NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(mUvBuffer.BeginWrite<float>(mHead * 4, inCount * 4));
                        inoutColorPtr = (IntPtr)NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(mColorBuffer.BeginWrite<float>(mHead * 4, inCount * 4));
                    }
                    mHead = (mHead + inCount) % MAX_COUNT;

                }
                else if (mTail > inCount)
                {
                    lPad += MAX_COUNT - mHead;
                    lLen += inCount;
                    mHead = 0;
                    unsafe
                    {
                        inoutTrasformPtr = (IntPtr)NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(mTransformBuffer.BeginWrite<float>(mHead * 4, inCount * 4));
                        inoutUvPtr = (IntPtr)NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(mUvBuffer.BeginWrite<float>(mHead * 4, inCount * 4));
                        inoutColorPtr = (IntPtr)NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(mColorBuffer.BeginWrite<float>(mHead * 4, inCount * 4));
                    }
                    mHead += inCount;
                }
            }
            else
            {
                if (mTail - mHead - 1 > inCount)
                {
                    lLen += inCount;
                    unsafe
                    {
                        inoutTrasformPtr = (IntPtr)NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(mTransformBuffer.BeginWrite<float>(mHead * 4, inCount * 4));
                        inoutUvPtr = (IntPtr)NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(mUvBuffer.BeginWrite<float>(mHead * 4, inCount * 4));
                        inoutColorPtr = (IntPtr)NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(mColorBuffer.BeginWrite<float>(mHead * 4, inCount * 4));
                    }
                    mHead += inCount;
                }
            }
            if (lLen > 0)
            {
                mPendingOp = new GraphicsFenceData();
                mPendingOp.setLen(lPad, lLen);
            }
        }
        
        public int endUpdate(int inCount)
        {
            if (mPendingOp != null)
            {
                mHead -= mPendingOp.updateLen(inCount);
                mTransformBuffer.EndWrite<float>(inCount * 4);
                mUvBuffer.EndWrite<float>(inCount * 4);
                mColorBuffer.EndWrite<float>(inCount * 4);
                if (inCount > 0)
                {
                    return mHead - inCount;
                }
                else
                {
                    mPendingOp = null;
                }
            }
            return -1;
        }
    }
    private RingBuffer mRingBuffer;

    /*/////////////////////////////*/
    // SubRegion-Updatable Texture 
    /*/////////////////////////////*/
    private class Spritesheet
    {
        private Texture2D mTex;

        public Texture2D getTexture()
        {
            return mTex;
        }
        public void initialize(int inWidth, int inHeight)
        {
            mTex = new Texture2D(inWidth, inHeight, TextureFormat.RGBA32, false);
        }
        public void load(int inWidth, int inHeight, int inOffsetX, int inOffsetY, IntPtr inData)
        {
            Texture2D lStagingTex = new Texture2D(inWidth, inHeight, TextureFormat.RGBA32, false);
            lStagingTex.LoadRawTextureData(inData, inWidth * inHeight * 4);
            lStagingTex.Apply();
            Graphics.CopyTexture(lStagingTex, 0, 0, 0, 0, inWidth, inHeight, mTex, 0, 0, inOffsetX, inOffsetY);
        }
    }
    private Spritesheet mSpritesheet;

    /*/////////////////////////////*/
    // Sprite Renderer Material
    // Reference
    /*/////////////////////////////*/
    [SerializeField]
    private Material mMaterial;

    [SerializeField]
    private Camera mMainCamera;

    /*/////////////////////////////*/
    // Simple Quad Mesh For Sprites
    /*/////////////////////////////*/
    private Mesh mQuadMesh;

    /*/////////////////////////////*/
    // Draw Indirect Args Dynamic
    // ComputeBuffer
    /*/////////////////////////////*/
    private ComputeBuffer mArgsBuffer;
    private uint[] mArgs;

    /*/////////////////////////////*/
    // Asteroid Plugin Callbacks
    /*/////////////////////////////*/
    private static void OnBeginFillRenderBuffers(ref AstBeginFillRenderBuffersCallbackData inCallbackData)
    {
        var lThis = (AsteroidMain)GCHandle.FromIntPtr(inCallbackData.mThis).Target;
        lThis.mRingBuffer.beginUpdate((int)inCallbackData.mObjectCount, ref inCallbackData.mTransformPtr, ref inCallbackData.mUvPtr, ref inCallbackData.mColorPtr);
    }

    private static void OnEndFillRenderBuffers(ref AstEndFillRenderBuffersCallbackData inCallbackData)
    {
        var lThis = (AsteroidMain)GCHandle.FromIntPtr(inCallbackData.mThis).Target;
        int lStartInstance = lThis.mRingBuffer.endUpdate((int)inCallbackData.mObjectCount);
        if (lStartInstance >= 0)
        {
            lThis.mArgs[1] = (uint)inCallbackData.mObjectCount;
            //lThis.mArgs[4] = (uint)lStartInstance;
            lThis.mArgsBuffer.SetData(lThis.mArgs);
            lThis.mMaterial.SetInt("_BaseInstanceId", lStartInstance);
            Graphics.DrawMeshInstancedIndirect(lThis.mQuadMesh, 0, lThis.mMaterial, new Bounds(Vector3.zero, new Vector3(((UInt32)Screen.width) >> 1, ((UInt32)Screen.height) >> 1, 20)), lThis.mArgsBuffer);
        }
        
    }

    private static void OnLoadTexture(ref AstLoadTextureCallbackData inCallbackData)
    {
        var lThis = (AsteroidMain)GCHandle.FromIntPtr(inCallbackData.mThis).Target;
        lThis.mSpritesheet.load((int)inCallbackData.mWidth, (int)inCallbackData.mHeight, (int)inCallbackData.mOffsetX, (int)inCallbackData.mOffsetY, inCallbackData.mData);
    }

    /*/////////////////////////////*/
    // Asteroid Unity
    /*/////////////////////////////*/

    private GCHandle mHandleToThis;
    void Start()
    {
        mHandleToThis = GCHandle.Alloc(this, GCHandleType.Pinned);
        astSetBeginFillRenderBuffersCallback(GCHandle.ToIntPtr(mHandleToThis), Marshal.GetFunctionPointerForDelegate(sOnBeginFillRenderBuffersDelegate));
        astSetEndFillRenderBuffersCallback(GCHandle.ToIntPtr(mHandleToThis), Marshal.GetFunctionPointerForDelegate(sOnEndFillRenderBuffersDelegate));
        astSetLoadTextureCallback(GCHandle.ToIntPtr(mHandleToThis), Marshal.GetFunctionPointerForDelegate(sOnLoadTextureDelegate));
        mSpritesheet = new Spritesheet();
        mSpritesheet.initialize(1024, 1024);
        mRingBuffer = new RingBuffer();
        mRingBuffer.initialize();
        mArgs = new uint[5] { 6, 0, 0, 0, 0 };
        mArgsBuffer = new ComputeBuffer(1, 5 * sizeof(uint), ComputeBufferType.IndirectArguments);
        mQuadMesh = new Mesh();
        Vector3[] vertices = new Vector3[4];
        vertices[0] = new Vector3(0, 0, 0);
        vertices[1] = new Vector3(1, 0, 0);
        vertices[2] = new Vector3(0, 1, 0);
        vertices[3] = new Vector3(1, 1, 0);
        mQuadMesh.vertices = vertices;
        int[] tri = new int[6];
        tri[0] = 0;
        tri[1] = 2;
        tri[2] = 1;
        tri[3] = 2;
        tri[4] = 3;
        tri[5] = 1;
        mQuadMesh.triangles = tri;
        Vector3[] normals = new Vector3[4];
        normals[0] = -Vector3.forward;
        normals[1] = -Vector3.forward;
        normals[2] = -Vector3.forward;
        normals[3] = -Vector3.forward;
        mQuadMesh.normals = normals;
        Vector2[] uv = new Vector2[4];
        uv[0] = new Vector2(0, 0);
        uv[1] = new Vector2(1, 0);
        uv[2] = new Vector2(0, 1);
        uv[3] = new Vector2(1, 1);
        mQuadMesh.uv = uv;
        this.mMaterial.mainTexture = mSpritesheet.getTexture();
        this.mMaterial.SetBuffer(Shader.PropertyToID("transformBuffer"), mRingBuffer.getTransformBuffer());
        this.mMaterial.SetBuffer(Shader.PropertyToID("uvBuffer"), mRingBuffer.getUvBuffer());
        this.mMaterial.SetBuffer(Shader.PropertyToID("colorsBuffer"), mRingBuffer.getColorBuffer());
        astEngineInitialize((UInt32)Screen.width, (UInt32)Screen.height);
        mMainCamera.orthographicSize = ((UInt32)Screen.height) >> 1;
    }

    void OnDestroy()
    {
        astEngineDispose();
        mRingBuffer.dispose();
        mArgsBuffer.Release();
        mHandleToThis.Free();
        Application.Quit();
    }
    
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Destroy(gameObject);
        }
        else
        {
            UInt32 lKeyState = 0;
            if (Input.GetKey("a")) 
            {
                lKeyState |= (UInt32)EKeyState.LEFT;
            }
            if (Input.GetKey("d")) 
            {
                lKeyState |= (UInt32)EKeyState.RIGHT;
            }
            if (Input.GetKey("w")) 
            {
                lKeyState |= (UInt32)EKeyState.UP;
            }
            if (Input.GetKey("s"))
            {
                lKeyState |= (UInt32)EKeyState.DOWN;
            }
            if (Input.GetKey("space"))
            {
                lKeyState |= (UInt32)EKeyState.SPACE;
            }
            astEngineUpdate(lKeyState);
        }
    }

    static AsteroidMain()
    {
        sOnBeginFillRenderBuffersDelegate = new OnBeginFillRenderBuffersDelegate(AsteroidMain.OnBeginFillRenderBuffers);
        sOnEndFillRenderBuffersDelegate = new OnEndFillRenderBuffersDelegate(AsteroidMain.OnEndFillRenderBuffers);
        sOnLoadTextureDelegate = new OnLoadTextureDelegate(AsteroidMain.OnLoadTexture);
    }
}
