cmake_minimum_required( VERSION 3.14 ) 
cmake_policy(SET CMP0091 NEW)

project( Asteroid CXX )

MACRO(MACRO_ENSURE_OUT_OF_SOURCE_BUILD MSG)
	 STRING(COMPARE EQUAL "${CMAKE_SOURCE_DIR}"
	 "${CMAKE_BINARY_DIR}" insource)
	 GET_FILENAME_COMPONENT(PARENTDIR ${CMAKE_SOURCE_DIR} PATH)
	 STRING(COMPARE EQUAL "${CMAKE_SOURCE_DIR}"
	 "${PARENTDIR}" insourcesubdir)
	IF(insource OR insourcesubdir)
		MESSAGE(FATAL_ERROR "${MSG}")
	ENDIF(insource OR insourcesubdir)
ENDMACRO(MACRO_ENSURE_OUT_OF_SOURCE_BUILD)

MACRO_ENSURE_OUT_OF_SOURCE_BUILD(
	"${CMAKE_PROJECT_NAME} requires an out-of-source build."
)

set(CMAKE_SKIP_INSTALL_ALL_DEPENDENCY ON)
set(CMAKE_CONFIGURATION_TYPES RelWithDebInfo)

#################################
add_subdirectory( 3rdparty )

#################################
add_subdirectory( plugin )
